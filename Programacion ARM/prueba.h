#!/bin/bash
PIC_MODEL=""
VDD=""
VDD_CMD=""
function Read {
	echo "$VDD_CMD"
	pk2cmd /P$PIC_MODEL /GP0-FF $VDD_CMD> /tmp/read.txt
	NUM=`cat /tmp/read.txt | wc -l`
	NUM_TAIL=`expr $NUM - 3`
	NUM_HEAD=`expr $NUM_TAIL - 4`	
	cat /tmp/read.txt | tail -n $NUM_TAIL | head -n $NUM_HEAD | awk '{print $1": " $2 "  " $3"  " $4 "  " $5"  " $6 "  " $7"  " $8 "  " $9}' | zenity --text-info --width 650 --height 500  "$READ_PIC" 
	rm /tmp/read.txt
}

function Write {
	zenity --info --text "          PICKIT 2 Linux          \n\n Selecciona un fichero Hexadecimal a importar"
	szSavePath=$(zenity --file-selection --file-selection --confirm-overwrite --text "Selecciona un fichero hexadecimal a importar");
	zenity --question --text "          PICKIT 2 Linux          \n\n Quieres grabar el fichero: \n$szSavePath ?"
	if [ "$?" = 0 ]; then
		stdout=`pk2cmd /P$PIC_MODEL -F$szSavePath -M`
		zenity --info --text "          PICKIT 2 Linux          \n\n $stdout"
	fi
	
}

function Verify	{
	stdout=`pk2cmd /P$PIC_MODEL -Y $VDD_CMD`
        echo "pk2cmd /P$PIC_MODEL -Y $VDD_CMD"
	zenity --info --text "          PICKIT 2 Linux          \n\n $stdout"

}

function Erase	{
	stdout=`pk2cmd /P$PIC_MODEL -E $VDD_CMD`
	zenity --info --text "          PICKIT 2 Linux          \n\n $stdout"
}

function Blanc_Check {
	stdout=`pk2cmd /P$PIC_MODEL -C $VDD_CMD`
        zenity --info --text "          PICKIT 2 Linux          \n\n $stdout"

}

function detect_pk2 {
	pk2cmd /?V | grep Firmw | awk '{print $4}'> /tmp/pkdetect
	detect=`cat /tmp/pkdetect`
	NOT_FOUND=`echo $detect | grep PICkit`
	if [ "$detect" = "PICkit" ]; then
		zenity --error --text "Programador no detectado, conecta el dispositivo, y reinicia la aplicación"
		rm /tmp/pkdetect
		exit 1
	fi
	rm /tmp/pkdetect 
}

function detect_mcu {
	PIC=`pk2cmd -P | head -n1`
	NOT_FOUND=`echo $PIC | grep "No known"`
	if [ "$NOT_FOUND" != "" ]; then
        	zenity --info --text "Microcontrolador no detectado, selecciona uno de la lista"
		LIST_PICS=`cat PICS.DAT`
		PIC_MODEL=`zenity  --list  --text "Pics soportados" --radiolist --height 400 --column "" --column "Model PIC" TRUE $LIST_PICS `	
	else
		PIC_MODEL=`echo $PIC | awk '{print $4}'| sed  's/\.//g'`
	fi
}

function set_vdd {
	VDD=`zenity  --list  --text "Selecciona Vdd" --radiolist  --height 275 --column "" --column "Vdd" TRUE Default FALSE "External Target" FALSE "5" FALSE "3.3" FALSE "2" FALSE "1"`
	if [ "$VDD" = "Default" ]; then
		VDD_CMD=""
	else
		if [ "$VDD" = "External Target" ]; then
			VDD_CMD="-W"
		else
	        	VDD_CMD=" -A$VDD"
		fi
	fi
}

######### BEGIN MAIN #############
detect_pk2
detect_mcu
set_vdd

i=1
while [ $i = 1 ]; do
	CONFIG=`zenity  --list  --text "               PICKIT 2 Linux         \n------------------------------------------------\nFirm:    $detect\nPIC:    $PIC_MODEL\nVdd:    $VDD\n------------------------------------------------" --radiolist  --height 375 --column " " --column "Operacion" TRUE Read FALSE Write  FALSE Erase FALSE BLANC_CHECK FALSE Configure FALSE Exit `
	if [ "$?" != 0 ]; then
                exit
	fi	

	if [ "$CONFIG" = "Read" ]; then
		Read
	fi

	if [ "$CONFIG" = "Write" ]; then
        	Write
	fi

	if [ "$CONFIG" = "Verify" ]; then
                Verify
        fi

	if [ "$CONFIG" = "Erase" ]; then
        	Erase
	fi

	if [ "$CONFIG" = "BLANC_CHECK" ]; then
        	Blanc_Check	
	fi
	
	if [ "$CONFIG" = "Configure" ]; then
                detect_mcu
		set_vdd
        fi


	if [ "$CONFIG" = "Exit" ]; then
        exit 0
	fi
done


/*
 * $Revision: 1.1 $
 */

#include "timer.h"
#include <iolpc2129.h>

static volatile int ms_ctr = 0;

void TimerBeat(void)
{
    // Called at 1000 Hz rate.
    ms_ctr++; // Sleep counter.
}

unsigned long v3;
void Sleep(int milliseconds)
{
    ms_ctr = 0;
    
    while (ms_ctr < milliseconds) ;
}

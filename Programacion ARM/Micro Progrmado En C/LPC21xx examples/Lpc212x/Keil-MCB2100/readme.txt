Example project for the Keil MCB2100 with a Philips LPC2129.
It shows basic use of parallel I/O, serial port, timer and the interrupt
controller.

The application flashes all 8 LED's.

The project is configured to use the J-Link JTAG interface and
downloads code into the on-chip flash memory.
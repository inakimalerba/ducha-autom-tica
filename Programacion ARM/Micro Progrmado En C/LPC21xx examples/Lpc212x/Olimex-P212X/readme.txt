
The workspace contains two example projects.
Both projects are configured for the Olimex LPC-P212x evaluation
board and the J-Link JTAG interface.

simple
This project switches on the yellow LED when button B1 is pressed and
the green LED when button B2 is pressed. Any character received on
CAN1 and CAN2 is echoed on the same port.

timer
This project demonstrates the use of interrupts and timer.
It toggles the yellow and green LED periodically.
Pressing button B1 switches on the yellow LED, pressing button B2
switches on the green LED.

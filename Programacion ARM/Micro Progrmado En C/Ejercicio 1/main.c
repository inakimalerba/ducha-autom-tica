/* main.c */
#include "main.h"
#define minuscula 'a'

int main (void)
{
  const unsigned char Minuscula = 'a';
  unsigned char origen[] = "El munDo de HOY";
  int i = 0;
  unsigned char *puntero = origen;
  
   /* Opcion 1 */
   while(origen[i]) {
	if(origen[i]>=Minuscula) {
	   origen[i] = origen[i] & ~0x20;
        }
       i++;
   }
   /* FIN Opcion 1 */
   i = 0;
   /* Opcion 2 */
   while(origen[i]) {
      if(origen[i]>=minuscula) {
	 origen[i] = origen[i] & ~0x20;
      }
      i++;
   }
   /* FIN Opcion 2 */

   /* Opcion 3 */
   while(*puntero) {
     if(*puntero>=minuscula) {
	*puntero &= ~0x20;
     }
     puntero++;
   }
   /* FIN Opcion 3 */
 
   /* Opcion 4  */
  asm ("otro: ldrb r1,[%0],#1    \n\t"
       "      cmp r1,#0          \n\t"
       "      beq salir          \n\t"
       "      cmp r1,%1          \n\t"
       "      andhs r1,#~0x20    \n\t"
       "      strhsb r1,[%0,#-1] \n\t"
       "      b otro             \n\t"
       "salir:                   \n\t"
      : /* output */
      :"r"(origen),"I"(minuscula)   /* input */
      :"r1","memory"
      ); 
  /* FIN Opcion 4 */
  return 0;
}


#include "adc.h"

void interrupcion(void) __attribute__ ((interrupt("IRQ")));
void inic_int_ADC(void);

unsigned int salida;

void inic_adc(void)
{	PINSEL0=0x00400000;//0000|0000|0100|0000|0000|0000|0000|0000
	salida=0;//Inicializo la salida
	ADCR=(Divisor_Clock_ADC|Modo_ADC_Operacional|Canal_1);//Escribo el ADC Register
	inic_int_ADC();
	ADCR|=(1<<24);//Comienza la conversion
}

void inic_int_ADC()
{	VICVectCntl0=(0x20|18);
	VICVectAddr0=(unsigned)interrupcion;
	VICIntSelect&=~(1<<18);
	VICIntEnable=(1<<18);
}

void interrupcion(void)
{salida=((ADDR>>6)&0x3FF);//Leo la salida de la conversion del ADC
 VICVectAddr=0x00000000;
 ADCR|=(1<<24);//Comienza la conversion de nuevo
}

unsigned int adc_salida(void)
{return salida;}






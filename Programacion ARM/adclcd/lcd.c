#include "lcd.h"

void inic_lcd_8bit()
{
	PINSEL0=0x00400000;//Defino las salidas P0.0 a P0.8 como GPIO
        IO0DIR=0x000007FF;//8 Bit de Datos más el bit de Enable, bit de RS y bit R/W
	IO0CLR=0x000007FF;//Pongo todas las salidas en 0	
        //RS|RW|ENABLE|(MSB)DB7|DB6|DB5|DB4|DB3|DB2|DB1|DB0(LSB)

	IO0SET=0x00000030;
	delay_us(Tiempo_Dato);//10ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=0x00000030;

	IO0SET=0x0000000C;
	delay_us(Tiempo_Dato);//10ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
 	delay_us(Tiempo_Dato);//10ms
	IO0CLR=0x0000000C;

        IO0SET=0x00000001;
	delay_us(Tiempo_Dato);//10 ms
	IO0CLR=ENABLE;//Habilito	
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=0x00000001;
	
	IO0SET=0x00000006;
	delay_us(Tiempo_Dato);//10 ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=0x000007FF;
}

void imprimir_lcd(unsigned char *Palabra)
{

 IO0SET=LIMPIAR;//Limpio el lcd
 delay_us(10);
 IO0SET=ENABLE;//Habilito
 delay_us(30);
 IO0CLR=ENABLE;//Desabilito
 delay_us(10);
 IO0CLR=LIMPIAR;//Pongo en bajo el bit DB0
 IO0SET=0x0000000C;//Caracteristicas de escritura del lcd
 delay_us(2000);
 IO0SET=ENABLE;//Habilito
 delay_us(30);
 IO0CLR=ENABLE;//Desabilito
 delay_us(10);
 IO0CLR=0x0000000C;//Pongo en bajo los bit anteriores
 IO0SET=0x00000080;//Direccion de memoria a escribir y de allí cada caracter se desplaza +1
 delay_us(10);
 IO0SET=ENABLE;//Habilito
 delay_us(30);
 IO0CLR=ENABLE;//Desabilito
 delay_us(100);
 IO0CLR=0x00000080;//Pongo en bajo el bit
 //Comienza el proseso de escribir las palabras

while(*Palabra!='\0')
	{
         IO0SET=(RS|*Palabra);
	 delay_us(10);
 	 IO0SET=ENABLE;
 	 delay_us(20);
	 IO0CLR=ENABLE;
	 delay_us(50);
 	 IO0CLR=(RS|0x00000FF);
 	 Palabra++;}
IO0CLR=RS;
}




#ifndef DELAY_H
#define DELAY_H
#include "lpc2114.h"
void inic_delay(void);
void inic_int(void);
void delay_us(unsigned int);
#endif

#ifndef LCD_H
#define LCD_H
#define Tiempo_Enable 100
#define Tiempo_Dato 10000
#define ENABLE 0x00000100
#define RS 0x00000400
#define RW 0x00000200
#define LIMPIAR 0x00000001
#define HOME 0x00000002
#include "lpc2114.h"
#include "delay.h"
void init_lcd_8bit(void);
void imprimir_lcd(char *);
#endif

#include "control.h"
/*ARCHIVO NO LISTO, FALTA LA OPCION DE CANCELAR PROCESO*/

unsigned int control(unsigned int tipo_soldadura)
{float y1, x1, x2, y2; //Puntos correspondientes a los extremos de la recta para calcular la funcion temp = pend*t + b
 float pendiente_A, pendiente_B, pendiente_C, corte_A, corte_B, corte_C; // Características de las rectas de soldadura de las etapa A y C
 float temp_teorica, temp_medida, tiempo; // Temperatura medida y la calculada teoricamente para el PID
 float porc, rango_pid; // Variables que permiten manejar el porcentaje del PWM
 unsigned int porc_pwm; //Cuentas del ADC para conversion a temperatura y Porcentaje de PWM a establecer
 unsigned char seguridad[]="Proceso Completo";
 unsigned char seguridad1[]="Retirar la placa";
 pwm_init();
/*Soldadura Personalizada*/
	if(tipo_soldadura==1)
		{
		/*Recta de la etapa A*/
		 temp_medida=muestreo();
		 y1=temp_medida;
		 x1=0;//Tiempo '0'

		//Paramétros finales Etapa A
		 y2=param_psl[3];//Temperatura etapa A
		 x2=param_psl[0];//Tiempo etapa A

		 pendiente_A=(y2-y1)/(x2-x1);//Calculo la pendiente
		 corte_A=y2-(pendiente_A*x2);//Calculo el corte al eje 'y'

		/*Recta de la etapa B*/
		 y1=param_psl[3];//La temperatura final de la etapa A
		 x1=param_psl[0];//Tiempo inicial de la etapa B

		//Paramétros finales Etapa B
		 y2=param_psl[6];//Temperatura etapa B
		 x2=param_psl[0]+param_psl[1];//Tiempo etapa B

		 pendiente_B=(y2-y1)/(x2-x1);//Calculo la pendiente
		 corte_B=y2-(pendiente_B*x2);//Calculo el corte al eje 'y'


		 /*Recta de la etapa C*/
		 y1=param_psl[6];//La temperatura final de la etapa B
		 x1=param_psl[0]+param_psl[1];// Tiempo de inicio de la etapa C

		 y2=param_psl[4];//Temperatura etapa C
		 x2=param_psl[0]+param_psl[1]+param_psl[2];//Tiempo etapa C

		 pendiente_C=(y2-y1)/(x2-x1);//Pendiente de la recta
		 corte_C=y2-(pendiente_C*x2);//Corte al eje 'y'

	/*COMIENZO DE TODO EL CONTROL*/
	 	inic_pid(0);//Inicializo el pid
		pwm_on();//Arranco el PWM con '0'
 		rango_pid=MAXIMO-MINIMO;//Calculo el rango del PID para calculo de porcentaje
//Etapa A
		pwm_modul(100);
		delay_ms(10000);
		inic_reloj();//Inicializo el reloj
		reloj();
		tiempo=0;//Inicializo el tiempo en '0'
		while(tiempo<param_psl[0])//Mientras sea menor el tiempo al tiempo final de la etapa A
			{tiempo=segundos;//Tiempo igual a los segundos del reloj
			 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 temp_teorica=(pendiente_A*tiempo)+corte_A;//Temp teórica a partir de la curva personalizada
			 porc=ctrl_pid(temp_medida, temp_teorica);//Obtenemos respuesta del PID para modular el pwm
			 porc_pwm=(porc*100)/rango_pid;//Conversion a porcentaje para el PWM corre entre 0% y 100%
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}
			 if(porc_pwm>=100)//Correcion para evitar error de 100 en PWM
				{porc_pwm=99;}
			 pwm_modul(porc_pwm);//Modulacion PWM para etapa de potencia en funcion resultado PID
			 visual_control(temp_medida, temp_teorica, 1);//Imprime informacion en LCD
			}//fin de while

//Etapa B
		x1=param_psl[0]+param_psl[1];//Calculamos tiempo final de la etapa B, sumando periodo de etapa A mas B
		while( (tiempo>=param_psl[0])&&(tiempo<x1))//Mientras estemos dentro de la etapa B hacemos lo siguiente
			{tiempo=segundos;
		 	 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 temp_teorica=(pendiente_B*tiempo)+corte_B;//La temperatura en la etapa B siempre es constante teoricamente
			 porc=ctrl_pid(temp_medida, temp_teorica);//Obtenemos respuesta del PID para modular el pwm
			 porc_pwm=(porc*100)/rango_pid;//Conversion a porcentaje para el pwm Corre entre 0% y 100%
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}
			 if(porc_pwm>=100) //Correcion para evitar error d 100 en PWM
				{porc_pwm=99;}
			 pwm_modul(porc_pwm);//Mandamos el porcentaje al PWM
			 visual_control(temp_medida, temp_teorica, 2);//Imrpimimos informacion en LCD
			 }
//Etapa C
		x2=x1+param_psl[2];//Definimos tiempo final de la etapa C, sumando etapa A, B y C
		while(((x1>=param_psl[0])&&(tiempo<x2))||(temp_medida<param_psl[4]))//Mientras estemos dentro del tiempo de la etapa C
			{tiempo=segundos;
			 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}
			if(tiempo<x2)
			{temp_teorica=(pendiente_C*tiempo)+corte_C;}//Calculo de la temperatura teorica
			else{temp_teorica=param_psl[4];}
			porc=ctrl_pid(temp_medida, temp_teorica);
			 porc_pwm=(porc*100)/rango_pid ;//Conversion a porcentaje para el pwm Corre entre 0% y 100%
			 if(porc_pwm>=100)//Correcion para evitar error d 100 en PWM
				{porc_pwm=99;}
			 pwm_modul(porc_pwm);
			 visual_control(temp_medida, temp_teorica, 3);
			}

//Etapa D
		pwm_off();
		while(tiempo>=x2)//Ingresamos en esta etapa cuando finalizamo la soldadura
			{visual_control(temp_medida, param_psl[5], 4);//Imprimimos informacion
			 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 tiempo=segundos;
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}
                         if(temp_medida<=param_psl[5])//Nos quedamos dentro del while hasta que el horno alcance la temp de seguridad
				return 0;
			 }

}//Fin del if soldadura personalizada

/*Soldadura Estandar*/
	else{
		 /*Recta de la etapa A*/
		 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
		 y1=temp_medida;
		 x1=0;//Tiempo '0'

		//Paramétros finales Etapa A
		 y2=param_std[3];//Temperatura etapa A
		 x2=param_std[0];//Tiempo etapa A

		 pendiente_A=(y2-y1)/(x2-x1);//Calculo la pendiente
		 corte_A=y2-(pendiente_A*x2);//Calculo el corte al eje 'y'

		/*Recta de la etapa B*/
		 y1=param_std[3];//La temperatura final de la etapa A
		 x1=param_std[0];//Tiempo inicial de la etapa B

		//Paramétros finales Etapa B
		 y2=param_std[6];//Temperatura etapa B
		 x2=param_std[0]+param_std[1];//Tiempo etapa B

		 pendiente_B=(y2-y1)/(x2-x1);//Calculo la pendiente
		 corte_B=y2-(pendiente_B*x2);//Calculo el corte al eje 'y'

		 /*Recta de la etapa C*/
		 y1=param_std[6];//La temperatura final de la etapa B
		 x1=param_std[0]+param_std[1];// Tiempo de inicio de la etapa C

		 y2=param_std[4];//Temperatura etapa C
		 x2=param_std[0]+param_std[1]+param_std[2];//Tiempo etapa C

		 pendiente_C=(y2-y1)/(x2-x1);//Pendiente de la recta
		 corte_C=y2-(pendiente_C*x2);//Corte al eje 'y'


		/*COMIENZO DE TODO EL CONTROL*/
	 	inic_pid(0);
		pwm_on();
		rango_pid=MAXIMO-MINIMO;
//Etapa A
		pwm_modul(100);
	        delay_ms(10000);
		inic_reloj();
		reloj();
		tiempo=0;
		while(tiempo<param_std[0])
			{tiempo=segundos;
		 	 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 temp_teorica=(pendiente_A * tiempo)+corte_A;
			 porc=ctrl_pid(temp_medida, temp_teorica);
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}
			 porc_pwm=(porc*100)/rango_pid;//Conversion a porcentaje para el pwm Corre entre 0% y 100%
			 if(porc_pwm>=100) //Correcion para evitar error d 100 en PWM
				{porc_pwm=99;}
			 pwm_modul(porc_pwm);
			 visual_control(temp_medida, temp_teorica, 1);
			 
			}
//Etapa B
		x1=param_std[0]+param_std[1];//Calculamos tiempo final de la etapa B, sumando periodo de etapa A mas B
		while( (tiempo>=param_std[0])&&(tiempo<x1))//Mientras estemos dentro de la etapa B hacemos lo siguiente
			{tiempo=segundos;
		 	 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 temp_teorica=(pendiente_B*tiempo)+corte_B;//La temperatura en la etapa B siempre es constante teoricamente
			 porc=ctrl_pid(temp_medida, temp_teorica);//Obtenemos respuesta del PID para modular el pwm
			 porc_pwm=(porc*100)/rango_pid;//Conversion a porcentaje para el pwm Corre entre 0% y 100%
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}			 
                         if(porc_pwm>=100) //Correcion para evitar error d 100 en PWM
				{porc_pwm=99;}
			 pwm_modul(porc_pwm);//Mandamos el porcentaje al PWM
			 visual_control(temp_medida, temp_teorica, 2);//Imrpimimos informacion en LCD
			 }
//Etapa C
		x2=x1+param_std[2];
		while(((x1>=param_std[0])&&(tiempo<x2))||(temp_medida<param_std[4]))
			{tiempo=segundos;
			 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}
			 if(temp_teorica<param_std[4])
			 {temp_teorica=(pendiente_C*tiempo)+corte_C;}
			 else{temp_teorica=param_std[4];}
			 porc=ctrl_pid(temp_medida, temp_teorica);
			 porc_pwm=(porc*100)/rango_pid;//Conversion a porcentaje para el pwm Corre entre 0% y 100%
			 if(porc_pwm>=100)//Correcion para evitar error d 100 en PWM
				{porc_pwm=99;}
			 pwm_modul(porc_pwm);
			 visual_control(temp_medida, temp_teorica, 3);
			}

//Etapa D
		pwm_off();
		while(tiempo>=x2)
			{visual_control(temp_medida, param_std[5], 4);
		 	 temp_medida=muestreo();//La temperatura actual del horno para inicio de la curva
			 tiempo=segundos;
			 if((IO0PIN&(1<<30))!=0)
				{pwm_off();return 4;}
			 if(temp_medida<=param_std[5])
				return 0;
			 }

}
imprimir_lcd(seguridad, 1);
imprimir_lcd(seguridad1, 2);
delay_ms(4000);
return 0;}



void visual_control(float temperatura, float temperatura1, unsigned int letra)//Imprime la informacion por la pantalla
{unsigned int aux, aux1, aux2; //Variables para la conversion de int a char
 unsigned char info1[]="Etapa   (   )[s]";
 unsigned char info2[]="Temp:   |    [C]";

switch(letra)//Imprime en info1 la letra correspondiente a la etapa
	{case 1: {info1[6]='A';break;}
	 case 2: {info1[6]='B';break;}
	 case 3: {info1[6]='C';break;}
	 case 4: {info1[6]='D';break;}}

/*SEGUNDOS*/
aux2=segundos;
aux=aux2/10;
aux1=aux2/100;
info1[9]=48+aux1;
info1[10]=48+(aux-(aux1*10));
info1[11]=48+(aux2-(aux*10));
imprimir_lcd(info1, 1);

/*Temperatura*/
aux=(unsigned int)temperatura;
aux1=(unsigned int)temperatura/100;
aux2=(unsigned int)temperatura/10;
info2[5]=48+aux1;
info2[6]=48+(aux2-(aux1*10));
info2[7]=48+(aux-(aux2*10));

/*Temperatura*/
aux=(unsigned int)temperatura1;
aux1=(unsigned int)temperatura1/100;
aux2=(unsigned int)temperatura1/10;
info2[9]=48+aux1;
info2[10]=48+(aux2-(aux1*10));
info2[11]=48+(aux-(aux2*10));
imprimir_lcd(info2, 2);
}


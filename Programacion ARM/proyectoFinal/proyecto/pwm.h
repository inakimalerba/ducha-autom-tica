#ifndef PWM_H
#define PWM_H
/*ARCHIVO LISTO*/
#include "lpc2114.h"
#define Prescaler 256 //Valores Prescaler 0: 1 ciclo -  1: 2 ciclos del pclk
#define Long_ciclo 0x000070B2//Calcular valor en funcion del prescaler y del pclk para obtener el tiempo de ciclo necesario
#define Duty_std 0x00003859//Valor para definir un posible duty d reposo (long_cicle/2)
void pwm_init (void); //Inicialización sistema
void pwm_on(void); // Encendido Pwm
void pwm_off(void); //Apagado Pwm
void pwm_modul(unsigned int);
#endif
/*Ciclo de pclk (Peripheral Clock) = 1 / Frec Cristal = 1 / 14.7654 MHz = 67.7 nseg
 Periodo señal PWM = 67.7 nseg * Prescaler * Long_ciclo (Arbitrario)
 Duty = Período * porcentaje*/

#include "pid.h"

void inic_pid(float error_inic)
{I=0;
 error_t_1=error_inic;//error_t_1 seria indicativo de error(t-1)
}

float ctrl_pid(float temp_m, float temp_r)
{float error_t0, D, u;

error_t0=temp_r-temp_m; //Resto la señal de referencia con la señal tomada de la termocupla

D = Qd * (error_t0 - error_t_1); //Calculo la parte Derivariva->Ponderacion*(DeltaError/Periodo)
I = I + (Qi * error_t0); //Calculo de la parte Integral->(Ponderacion*Error)+Integralpasada

u = (Kp * error_t0) + D + I; //Sumatoria de la parte proporcional, derivativa e integral

if((u>MAXIMO)||(u<MINIMO)) //Limito la parte integral
	I = I - (Qi * error_t0);
if(u>MAXIMO) //Para mantener la amortiguación estable
	u = MAXIMO;
if(u<MINIMO) //Para mantener la amortiguacion estable
	u = MINIMO;

error_t_1 = error_t0; //Actualizo la señal de error de la muestra anterior

return u;
}





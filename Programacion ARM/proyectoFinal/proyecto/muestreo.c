#include "muestreo.h"

float muestreo(void)
{float muestras[CANT_MUESTRA];
 unsigned int cnta, i, j, posicion, preacum, acum, copy[CANT_MUESTRA], ayuda;

for(i=0; i<CANT_MUESTRA; i++)
	{cnta=adc_salida(1);
	 muestras[i]=cnta*CONSTANTE_TERMO;
	 copy[i]=(unsigned int)muestras[i];
	 delay_ms(TIEMPO_MUESTREO);}

posicion=0;
preacum=0;

for(i=0; i<CANT_MUESTRA; i++)
	{acum=0;
         ayuda=(unsigned int)muestras[i];
	 	for(j=0; j<CANT_MUESTRA; j++)
			{if(ayuda==copy[j])
				acum++;}
 		if(acum>preacum)
			{posicion=i;
			 preacum=acum;}}

if(muestras[posicion]<=35)
	{muestras[posicion]=muestras[posicion]+5;}
else{if(muestras[posicion]>=160)
		{muestras[posicion]=muestras[posicion]+10;}}

return muestras[posicion];
}

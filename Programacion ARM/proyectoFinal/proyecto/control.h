#ifndef CONTROL_H
#define CONTROL_H
#include "main.h"
#include "pid.h"
#include "delay.h"
#include "reloj.h"
#include "lcd.h"
#include "muestreo.h"
/*ARCHIVO LISTO*/
extern unsigned int param_psl[7];
extern unsigned int param_std[7];
extern unsigned int segundos;
extern float u_max, u_min;
unsigned int control(unsigned int);
void visual_control(float, float, unsigned int);
#endif

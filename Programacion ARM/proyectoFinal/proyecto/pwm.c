#include "pwm.h"
/*ARCHIVO LISTO*/
void pwm_init(void){

//Configuración registros
PWMPR = Prescaler; // Valor de prescaler para el incremento del Time Counter (TC), definido en el header
PWMMCR = 0x00000002; // Registro de control de match: configurado asi resetea el TC cuando cumple el ciclo
PWMPCR = 0x00002000; //Registro control de salidas: establece PWM5 controlado por single edge y la habilita
PWMMR0 = Long_ciclo; //Match Reg 0 establece la longitud del ciclo PWM en cuentas. Un aumento de una cuenta corresponde a:
		     // TC + 1 = (PR+1) cycles of pclk
PWMMR5 = Duty_std;  // Valor entre 0 y Long_ciclo definido como estandar por el sistema para obtener un duty de cierto valor (50%)
PWMLER = 0x00000021; //Activa los latch para permitir la carga de datos en MMR0 y MMR5
}

void pwm_on(void){
PWMTCR = 0x00000002;    //Reset de Contador TC y Prescaler
PWMTCR = 0x00000009;    //Habilitacion de modo PWM e inicio del funcionamiento
}

void pwm_off(void){
PWMMR5 = 0;
PWMLER = 0x00000020;
}

void pwm_modul(unsigned int porcentaje)
{
unsigned int var;
if((porcentaje <= 100)&&(porcentaje>0))
{
var =(Long_ciclo * porcentaje)/ 100; // Calculo el valor a cargar para obtener un PWM del Porcentaje requerido respecto al total de la long del ciclo
PWMMR5 = var;
PWMLER = 0x00000020; // En el próx ciclo actualiza el valor de MMR5 para modificar el pwm
}
}


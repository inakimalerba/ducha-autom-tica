#ifndef PID_H
#define PID_H
#define MAXIMO 250
#define MINIMO 3
#define Kp 25
#define Td 0.02
#define Ti 416.67
#define T 0.5
#define Qd Kp*(Td/T)
#define Qi Kp*(T/Ti)
void inic_pid(float error);//Inicializa el pid
float ctrl_pid(float temp_m, float temp_r);//PID andando
float u, error_t_1, I;
#endif

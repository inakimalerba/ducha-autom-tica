\select@language {spanish}
\contentsline {section}{\numberline {1}Resumen}{1}
\contentsline {section}{\numberline {2}Marco Te\IeC {\'o}rico}{2}
\contentsline {subsection}{\numberline {2.1}Termocupla}{2}
\contentsline {subsection}{\numberline {2.2}Soldadura por reflow}{3}
\contentsline {section}{\numberline {3}Hardware}{5}
\contentsline {subsection}{\numberline {3.1}Horno}{5}
\contentsline {subsection}{\numberline {3.2}Adaptador de se\IeC {\~n}al para termocupla}{5}
\contentsline {subsubsection}{\numberline {3.2.1}Circuito}{6}
\contentsline {subsection}{\numberline {3.3}Pulsadores de Control y Leds de se\IeC {\~n}alizaci\IeC {\'o}n}{7}
\contentsline {subsubsection}{\numberline {3.3.1}Pulsadores}{7}
\contentsline {subsubsection}{\numberline {3.3.2}Leds de se\IeC {\~n}alizaci\IeC {\'o}n}{7}
\contentsline {subsection}{\numberline {3.4}Control de Potencia}{9}
\contentsline {section}{\numberline {4}Software}{10}
\contentsline {subsection}{\numberline {4.1}Microcontrolador - Herramientas de programaci\IeC {\'o}n y simulaci\IeC {\'o}n}{10}
\contentsline {subsection}{\numberline {4.2}Firmware desarrollado}{10}
\contentsline {subsubsection}{\numberline {4.2.1}Main}{10}
\contentsline {subsubsection}{\numberline {4.2.2}LCD}{11}
\contentsline {subsubsection}{\numberline {4.2.3}DELAY}{11}
\contentsline {subsubsection}{\numberline {4.2.4}ADC}{12}
\contentsline {subsubsection}{\numberline {4.2.5}LED}{12}
\contentsline {subsubsection}{\numberline {4.2.6}ERROR}{13}
\contentsline {subsubsection}{\numberline {4.2.7}CHEQUEO}{13}
\contentsline {subsubsection}{\numberline {4.2.8}PWM}{13}
\contentsline {subsubsection}{\numberline {4.2.9}MENU}{14}
\contentsline {subsubsection}{\numberline {4.2.10}RELOJ}{14}
\contentsline {subsubsection}{\numberline {4.2.11}CONTROL}{15}
\contentsline {subsection}{\numberline {4.3}PID Digital}{15}
\contentsline {section}{\numberline {5}Mediciones}{17}
\contentsline {subsection}{\numberline {5.1}Relevamiento Horno}{17}
\contentsline {subsection}{\numberline {5.2}Relevamiento Funcionamiento Est\IeC {\'a}ndar}{19}
\contentsline {section}{\numberline {6}Conclusi\IeC {\'o}n}{19}

#include "delay.h"

void intimer(void) __attribute__((interrupt("IRQ")));//Defino rutina cuando se genere una interrupcion en el timer0
unsigned int bandera;//Variable que permite salir del while de la funcion delay_us, que se logra al generar la interrupcion

void inic_int(void)
{VICVectCntl1=(0x20|4);//Asigno el vector 1 a la interrupcion del timer0
 VICVectAddr1=(unsigned)intimer;//Defino la direccion a la que debe saltar el uC al suceder la interrupcion
 VICIntEnable=(1<<4);//Habilito la interrupcion del timer0
 VICIntSelect&=~(1<<4);//La defino como tipo IRQ
}

void inic_delay(void)
{T0TCR=0;//Contador apagado
 T0TC=0;//Pongo en 0 el Timer Counter
 T0PC=0;//Pongo en 0 Prescale Counter
 T0MCR=7;//Habilito la interrupcion del MR0 y que resetee
 inic_int();//Funcion de inicializar todo lo referido a la interrupcion
}

void delay_us(unsigned int valorus)
{T0MR0=valorus;//Valor de espera en us
 bandera=0;//Se asigna salir para que quede esperando el programa en el while
 T0PR=14;//Prescale Register para que TC se incremente cada 1us(CLOCKMicro 14.1456MHz)
 T0TCR=1;//Arranca el timer0
 while(bandera==0);//Queda esperando
 T0TCR=0;}//Se detiene el timer0

void delay_ms(unsigned int valorms)
{T0MR0=valorms;//Valor de espera en us
 bandera=0;//Se asigna salir para que quede esperando el programa en el while
 T0PR=14745;//Prescale Register para que TC se incremente cada 1ms(CLOCKMicro 14.1456MHz)
 T0TCR=1;//Arranca el timer0
 while(bandera==0);//Queda esperando
 T0TCR=0;}//Se detiene el timer0

void intimer(void)
{bandera=1;//Asigno un nuevo valor a salir para que en delay_us salga del while
 VICVectAddr=0x00000000;//Finalizo el IRQ, para que no siga viendo el micro las demas interrupciones vectoriales
 T0PR=0;
 T0TC=0;//Vuelvo a 0 el TC
 T0PC=0;//Vuelvo a 0 el PC
 T0IR=1;}//Reseteo la bandera de interrupcion del MR0


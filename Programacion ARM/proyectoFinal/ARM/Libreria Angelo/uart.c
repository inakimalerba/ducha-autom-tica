#include "uart.h"

/*INICIALIZA LA UART*/

void UART_Init()
{
   PINSEL0 |= 0x00000005;    //Defino los pines como Tx y Rx
   U0IER = 0x0;             // Deshabilita todas las interrupciones
   U0IIR = 0x0;             // Borro identificaciones de interrupciones
   U0RBR = 0x0;             // Borra el "receive register"
 }
/*Inicializar Baud Rate*/
void UART_BaudRateConfig(unsigned int BaudRate)
{
   U0LCR |= (1<<7);//DLAB en 1;
   U0DLL = (unsigned char) (BaudRate >> 0);//Defino el byte más bajo del divisor
   U0DLM = (unsigned char) (BaudRate >> 8);//Defino el byte más alto del divisor
   U0LCR &= ~(1<<7);//DLAB en 0;
   U0LCR = 0x03;//00000011 8 bits, sin paridad y 1 bit de stop
   U0FCR = 0x07;//00000111 activar fifo, reset de fifo Tx y Rx, trigger FIFO Rx = 1 caracter
}
/*Enviar un byte*/
void UART_ByteSend(unsigned char *Data)
{
  if (((U0FCR>>0)&1)==1)//Si la FIFO está habilitada.
  {
    while(((U0LSR>>5)&1)==0);//Esperar hasta que al menos 1 posición de la FIFO esté libre.
  }
  else//Si la FIFO no está habilitada.
  {
    while(((U0LSR>>6)&1)==0);//Esperar hasta que el shift register del transmisor esté vacío.
  }
  U0THR=*Data;
}

/*Enviar cadena*/
void UART_StringSend(unsigned char *data)
{
  int i;
  unsigned char enter = 13;
  for (i = 0;;i++) {
    if(data[i] == 0) {
      UART_ByteSend(&enter);
      break;
    } else {
      UART_ByteSend(&data[i]);
    }
  }
}





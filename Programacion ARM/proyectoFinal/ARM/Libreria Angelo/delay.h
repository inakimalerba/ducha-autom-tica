#ifndef DELAY_H
#define DELAY_H
#include "lpc2114.h"
void inic_delay(void);//Inicializa el delay, se debe ejecutar antes de usar el delay_us()
void inic_int(void);//Inicializa la interrupcion pero se ejecuta sola en el inic_delay()
void delay_us(unsigned int);//Funcion que espera hasta la interrupcion 1us en realidad es 1.2us
#endif

#include "adc.h"

void interrupcion(void) __attribute__ ((interrupt("IRQ")));//Actualiza el valor de salida de la función adc_salida()
void inic_int_ADC(void);

unsigned int salida;

void inic_adc(void)
{	PINSEL0=0x00400000;//0000|0000|0100|0000|0000|0000|0000|0000 Defino a el P0.27 como pin de ADC
	salida=0;//Inicializo la salida como 0
	ADCR=(Divisor_Clock_ADC|Modo_ADC_Operacional|Canal_1);//Escribo el ADC Register
	inic_int_ADC();//Funcion que se encarga de activar la interrupcion del ADC
	ADCR|=(1<<24);//Comienza la conversion
}

void inic_int_ADC()
{	VICVectCntl0=(0x20|18);//Defino el vector0 para que se encargue de la interrupcion del ADC
	VICVectAddr0=(unsigned)interrupcion;//Defino la direccion que debe saltar el uC al suceder la interrpcion
	VICIntSelect&=~(1<<18);//La defino como tipo IRQ
	VICIntEnable=(1<<18);//La habiilito a la interrupcion
}

void interrupcion(void)
{salida=((ADDR>>6)&0x3FF);//Leo la salida de la conversion del ADC y la asigno a salida
 VICVectAddr=0x00000000;//Finalizo la IRQ
 ADCR|=(1<<24);//Comienza la conversion de nuevo
}

unsigned int adc_salida(void)
{return salida;}//Cuenta del ADC

#include <iostream>

#include <SerialPort.h>
#define Velocidad BAUD_9600
#define Tam_Palabra CHAR_SIZE_8
#define Paridad PARITY_NONE
#define Bit_Parada STOP_BITS_1
#define Control FLOW_CONTROL_NONE

using namespace std;

string CMD= "ANDA LA PUTA MADRE";

int main(int argc, char **argv)
{
int leer=1;


  //Chequea la conexión con el dispositivo serie
  if (argc<2) {
    cerr << "No se encontro dispositivo Serie" << endl;
    return 1;
  }

  cout << "Serial port: " << argv[1] << endl;
  cout << "Abriendo...." << endl;

  //-- Open the serial port
  //-- Serial port configuration: 9600 baud, 8N1
  SerialPort serial_port(argv[1]);
  try {
    serial_port.Open(SerialPort::Velocidad,
                     SerialPort::Tam_Palabra,
                     SerialPort::Paridad,
                     SerialPort::Bit_Parada,
                     SerialPort::Control);
  }
  
  catch (SerialPort::OpenFailed E) {
    cerr << "Error al configurar el serial port" << endl;
    return 1;
  }
  
 cout<<"Configuración por defecto"<<endl;
 cout<<"Velocidad de comunicación: 9600 baudios/s"<<endl<<"Tamaño Palabra: 8 Bit"<<endl<<"Paridad: Sin"<<endl<<"Bit Parada: 1 Bit"<<endl<<endl;

 serial_port.Write(CMD);

while(leer==1)
  {//Esperar para recibir
  SerialPort::DataBuffer buffer;
 
  try {
    serial_port.Read(buffer, CMD.size() ,500);
  }
  catch (SerialPort::ReadTimeout E) {
    cout <<endl<<"TIMEOUT!";
    leer=0;
  }

  //-- Show the received data
  for(int i=0; i<buffer.size(); i++) {
  cout << buffer[i];}
  cout << endl;
 }
  cout<<"Puerto Cerrado"<<endl;
  serial_port.Close();
  return 0;

 }

#include "pid.h"

void inic_pid(float MAXIMO, float MINIMO, float error)
{
 umax=MAXIMO;
 umin=MINIMO;
 e1=error;
}

float ctrl_pid(float y, float r)
{
float e0, D, u;

e0=r-y; //Resto la señal de referencia con la señal tomada de la termocupla

D=qd*(e0-e1); //Calculo la parte Derivariva->Ponderacion(DeltaError/Periodo)
I=I+(qi*e0); //Calculo de la parte Integral->(Ponderacion*Error)+Integralpasada

u=(Kp*e0)+D+I; //Sumatoria de la parte proporcional, derivativa e integral

if((u>umax)||(u<umin)) //Limito la parte integral
	I=I-(qi*e0);
if(u>umax) //Para mantener la amortiguación estable
	u=umax;
if(u<umin) //Para mantener la amortiguacion estable
	u=umin;

e1=e0; //Actualizo la señal de error de la muestra anterior

return u;
}





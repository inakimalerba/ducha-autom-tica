#ifndef ADC_H
#define ADC_H
#include "lpc2114.h"
#define Divisor_Clock_ADC (16<<8)
#define Modo_ADC_Operacional (1<<21)
#define Canal_1 (2<<0)
unsigned int adc_salida(void);
void inic_adc(void);
#endif

#include "adc.h"
/*ARCHIVO LISTO*/
unsigned int adc_salida(unsigned int canal){
    unsigned int salida;
    //if(canal==3)
	    //canal=0x4;
    ADCR=(Divisor_Clock_ADC|Modo_ADC_Operacional|canal);//Lo enciendo y elijo el canal
    ADCR|=(1<<24);

    do{
        salida=ADDR;
    }while( (salida & (1<<31)) ==0 );//No sale hasta terminar la conversion

	salida=( (salida>>6) &0x3FE );
    // ADCR&=Modo_ADC_Low;//Pongo en modo low
    
    return salida;
}//Devuelvo la cuenta de la conversion

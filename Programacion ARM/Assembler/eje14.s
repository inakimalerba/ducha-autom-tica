.text
.arm
.global _start
_start:


b main

funcion:
stmfd r13!, {r0, r1, r3}
mov r2, #4
mov r3, #0
sub r0, r2

bucle:
ldrb r1, [r0], #1
add r3, r1
subs r2, #1
bne bucle
mov r2, r3, lsr #2
ldmfd r13!, {r0, r1, r3}
mov pc, lr

main:
mov r0, #datos
add r3, r0, #4
mov r1, #prom

programa:
ldrb r2, [r0], #1
cmp r2, #0
beq salir
cmp r3, r0
blls funcion
strb r2, [r1], #1
b programa

salir:
strb r2, [r1]
b loop

loop:
b loop


datos: .byte  7, 3, 6, 5, 9, 2, 2, 8, 9, 7, 6, 5, 3, 2, 1, 3
prom: .space(16)
.end

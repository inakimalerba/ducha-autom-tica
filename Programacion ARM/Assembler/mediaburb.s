.text
.arm
.global _start
_start:
         b reset
 
 reset:
 mov r1, #150
 mov r2, #10
 mov r3, #75
 mov r4, #45
 mov r5, #25
 
 ciclo:
 cmp r4,r5
 bhi et1
 cmp r3, r4
 bhi et2
 cmp r2, r3
 bhi et3
 cmp r1, r2
 bhi et4
 
b loop
et1:
mov r6, r4
mov r4, r5
mov r5, r6
b ciclo
et2:
mov r6, r3
mov r3, r4
mov r4, r6
b ciclo 
et3:
mov r6, r2
mov r2, r3
mov r3, r6
b ciclo 
et4:
mov r6, r1
mov r1, r2
mov r2, r6
b ciclo 
 loop:   b loop
 CTE1: .word 0x1231AAAA
 .end

.text
.arm
.global _start
_start:
         b reset
 
 reset:
 mov r1, #0
 mov r2, #100
 
 et1:
 cmp r1, r2
 bcc ciclo
 
 b loop 

 ciclo:
 add r1, r1, #1
 b et1
 
 loop:   b loop
 CTE1: .word 0x1231AAAA
 .end

.text
.arm
.global _start
_start:

ldr r4, =vect1
mov r0, #8
mov r1, #8

For1:
	subs r0, #1
	bne For2
	b loop1
For2:	
	subs r1, #1
	bne comparar
	beq actualizar

comparar:
	ldrsh r2, [r4], #2
	ldrsh r3, [r4]
	cmp r2, r3
	bge cambiar
yacambiado:
	strh r3, [r4] 
	sub r4, #2	
	strh r2, [r4], #2
	b For2	

actualizar:
	mov r1, #7
	ldr r4, =vect1
	b For1

cambiar:
	mov r6, r3
	mov r3, r2
	mov r2, r6
	b yacambiado	
	

loop1:
ldr r0, =vect1
ldrsh r1, [r0], #2 
ldrsh r2, [r0], #2
ldrsh r3, [r0], #2
ldrsh r4, [r0], #2
ldrsh r5, [r0], #2
ldrsh r6, [r0], #2
ldrsh r7, [r0], #2
ldrsh r8, [r0]
loop: b loop

vect1: .hword 10, -1, 1, 5, -5, 8, 2, 7 
.end


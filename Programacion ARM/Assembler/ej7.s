.text
.arm
.global _start
_start:

	ldr r1, =vect
	mov r5, r1	
	mov r0, #0
	mov r2, #16
	
	suma:
        ldrsh r3, [r1], #2
	add r0, r0, r3
	subs r2, #1
	bne suma

	mov r1, r5
	lsr r0, #4	
	mov r2, #15
	ldrsh r4, [r1], #2
        
	comp:
	ldrsh r3, [r1], #2
	cmp r3, r4
	movge r4, r3
	subs r2, #1
	bne comp 
	
        mov r1, r5
	mov r2, #17

	comparar:
	ldrsh r3, [r1], #2	
	subs r2, #1
	beq loop	
	cmp r3, r0
	beq listo	
	bge mayor
	ble menor
	b comparar

mayor: 
	cmp r3, r4
	bge comparar
	mov r4, r3
	b comparar
menor: 
	cmp r3, r4
	ble comparar
	mov r4, r3
	b comparar
listo:
mov r4, r3
loop: b loop
vect: .hword -1, 0, 10, 20, -5, -23, 20, 30, 50, 30, 20, -40, 2, 4, 5, 8
.end
	

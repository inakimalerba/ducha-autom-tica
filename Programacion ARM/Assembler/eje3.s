.text
.arm
.global _start
_start:

ldr r1, valor
ldr r2, valor1
ldr r3, valor2
mov r7, #0

continuar:
add r7, #1
cmp r7, #1
beq igualinclusive
cmp r7, #2
beq igualexclusive
cmp r7, #3
beq comparary
cmp r7, #4
beq compararo
b loop

compararo:
cmp r1, #100
blt verdadero
cmp r2, #20
bgt verdadero
b falso

comparary:
cmp r1, #100
bgt falso
cmp r1, #20
bgt verdadero 
b falso

igualexclusive:
cmp r1, #10
beq verdadero
cmp r1, #15
beq verdadero
cmp r1, #20
beq verdadero
b falso

igualinclusive:
cmp r1, #10
cmpeq r2, #15
cmpeq r3, #20
beq verdadero
b falso

verdadero:
mov r0, #1
b continuar

falso:
mov r0, #0
b continuar

loop:
b loop
valor: .word 20
valor1: .word 18
valor2: .word 5
.end

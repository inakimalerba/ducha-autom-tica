.text
.arm
.global _start
_start:

b main

codificar:
ldrb r3, [r1], #1
cmp r3, #0
beq reset
add r2, r3
mov pc, lr

reset:
mov r1, #codigo
b codificar

main:
mov r0, #vect
mov r1, #codigo
bucle:
ldrb r2, [r0]
cmp r2, #0
beq salir
bl codificar
strb r2, [r0], #1
b bucle

salir:
strb r2, [r0]
b loop

loop:
b loop

vect: .asciz "Palabras a encriptar"
codigo: .asciz "af12"
.end

.text
.arm
.global _start
_start:

	mov r1, #15
        mov r2, #200
        mov r3, #15
        
@Primera: r1<100 y r1>20

	cmp r1, #100
        bge no_1
        cmp r1, #20
        bls no_1
        mov r0, #1
        b siguiente_1

no_1: mov r0, #0
siguiente_1:

@Segunda: r1<100 o r2>20

	cmp r1, #100
        ble si_2
        cmp r2, #20
        bge si_2
        mov r5, #(0<<1)
        orr r0, r0, r5
        b siguiente_2
si_2:mov r5, #(1<<1) 
     orr r0, r0, r5
siguiente_2:
@Tercera: r1 = 10 o r1 = 15 o r1 = 20

	cmp r1, #10
        beq si_3
        cmp r1, #15
        beq si_3
        cmp r1, #20
        beq si_3
        mov r5, #(0<<2)
        orr r0, r0, r5
        b siguiente_3
si_3:mov r5, #(1<<2) 
     orr r0, r0, r5
siguiente_3:
@Cuarta: r1 = 10 y r2 = 15 y r3 = 20
	cmp r1, #10
        bne no_4
        cmp r2, #15
        bne no_4
	cmp r3, #20
        bne no_4
        mov r5, #(1<<3)
	orr r0, r0, r5
        b siguiente_4
no_4:mov r5, #(0<<3)
     orr r0, r0, r5
siguiente_4:

loop: b loop

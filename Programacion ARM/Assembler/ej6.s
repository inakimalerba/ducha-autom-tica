.text
.arm
.global _start
_start:

	ldr r1, =vect
	mov r2, #16
        mov r3, #0

        suma:
	 ldrsh r0, [r1], #2
	 adds r3, r0
	 subs r2, #1
	 beq siguiente
	 b suma
	siguiente:
	lsr r3, #4

loop: b loop
vect: .hword 10, -5, 4, 2, 15, -1, 34, 25, 30, 3, -5, 21, 34, 17, 29, 40

.end

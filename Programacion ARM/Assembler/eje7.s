.text
.arm
.global _start
_start:

mov r0, #vector
mov r1, #cantidad
ldr r3, [r1]
mov r2, #0

sumar:
     ldrsh r4, [r0], #2
     adds r2, r4
     sub r3, #1
     cmp r3, #0
     beq promedio
b sumar

promedio:
mov r2, r2, lsr #4

loop:
b loop
cantidad: .word 16
vector: .hword 10,20,30,40,50,60,70,80,90,100,110,120,130,140,-10,-20
.balign 4
.end

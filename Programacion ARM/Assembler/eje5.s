.text 
.arm
.global _start
_start:

ldr r1, base
ldr r2, exponente

potencia:
add r3, r1
sub r2, #1
cmp r2, #0
beq loop
b potencia

loop:
b loop

base: .word 4
exponente: .word 4
.end

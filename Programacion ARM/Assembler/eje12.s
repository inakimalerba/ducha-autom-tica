.text
.arm
.global _start
_start:


b main

funcion1:
stmfd r13!, {r0, r1,  r2}
sub r2, #1
cambiar:
ldrb r1, [r0]
ldrb r3, [r0, r2]
strb r1, [r0, r2]
strb r3, [r0]
sub r2, #2
add r0, #1
add r5, r2, r0
sub r4, r5, r0
cmp r4, #0
bgt cambiar
ldmfd r13!, {r0, r1, r2}
add r0, r2, #1
mov r2, #0
mov pc, lr


main:
ldr r0, =vector
mov r2, #0
bucle:
	ldrb r1, [r0, r2]
	cmp r1, #32
	bleq funcion1
	add r2, #1
	cmp r1, #0
	bne bucle
	bl cambiar
loop:
b loop

vector: .asciz "Las Palabras son estas"
.end


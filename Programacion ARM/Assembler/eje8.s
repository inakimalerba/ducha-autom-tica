.text
.arm
.global _start
_start:

mov r0, #vdirecciones
mov r1, #vdatos
mov r2, #10
mov r5, #0

otro:
ldrb r3, [r0], #1
ldr r4, [r1, r3, lsl #2]
add r5, r4
sub r2, #1
cmp r2, #0
beq loop
b otro

loop: 
b loop

vdirecciones: .byte 0, 4, 9, 3, 5, 6, 7, 5, 1, 2, 8
	      .balign 4

vdatos: .word 10, 20, 30, 40, 50, 60 ,70, 80, 90, 100
.end

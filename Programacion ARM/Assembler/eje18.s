.text
.arm
.global _start
_start:

b main

funcion:
stmfd r13!, {r0, r1, r2, r4}
sub r5, #2
add r1, #1
cambio:
      ldrb r0, [r1], #1
      cmp r0, #0
      beq restauro
      strb r0, [r2, r5]
      add r5, #1
b cambio
restauro:
ldmfd r13!, {r0, r1, r2, r4}
mov pc, lr

main:
mov r0, #vector1
ldr r1, =vector2
mov r2, #vector3
ldrb r4, [r1]
mov r5, #0
bucle:
	ldrb r3, [r0], #1
	cmp r3, #0
	beq salir
        strb r3, [r2, r5]
	add r5, #1
	cmp r3, r4
	bleq funcion
b bucle

salir:
mov r1, #0
add r5, #1
strb r1, [r2, r5]
b loop

loop:
b loop

vector1: .asciz "Insertar en 1 la palabra 1 "
vector2: .asciz "1mundo"
vector3: .space(50)
.balign 4
.end

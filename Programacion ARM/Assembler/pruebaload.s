/*Acceso a memoria--> ldr(load) y str(store)*/
/*Carga la información de una memoria a un registro o viceversa*/
/*Práctico cargar dos números desde la memoria, sumarlo y guardarlo en otra sección de memoria*/
/*Ejemplo:
 
ldr 'registro', ['registro puntero']
Va a buscar en la dirección de memoria guardada en el registro puntero al registro
*/

.text
.arm
.global _start
_start:

 mov r4, #123
 mov r3, #0
 mov r2, #200
 str r4, [r2]
 ldr r1, [r2]
 cmp r1, r4
 beq si
 b loop

 si:
 mov r3, #1

 loop:   b loop
 /*Como definir variables*/
 /*
 var1: .word 0x1000
 En este caso var1 indica donde se encuentra guardado en código exadecimal 1000
 Para leerlo seria:
 mov r1, #var1
 ldr r2, [r1]
Agrego esto:
 ldr r2, =r1
El igual se encarga de indicar a que apunta r1, facilita el proceso de la parte mov r1, #var1
 ldr r3, r1
*/ 

 CTE1: .word 0x1231AAAA
 .end

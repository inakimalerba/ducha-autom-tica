.text
.arm
.global _start
_start:

mov r0, #vector

pasar:
	ldrb r1, [r0]
	cmp r1, #0
	beq loop
	cmp r1, #32
	bne anal
	cmpeq r2, #3
	bls arreglar
	movgt r2, #0
	add r0, #1
	b pasar

anal:
cmp r2, #0
beq mayuscula
cmp r1, #97
addlt r1, #32
strb r1, [r0], #1
add r2, #1
b pasar

arreglar:
ldrb r1, [r3]
add r1, #32
strb r1, [r3]
mov r2, #0
add r0, #1
b pasar

mayuscula:
cmp r1, #97
mov r3, r0
subge r1, #32
strb r1, [r0], #1
add r2, #1
b pasar

loop:
b loop
vector: .asciz "hOlA pUTO de MieRDA"
.end

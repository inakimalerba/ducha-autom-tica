.text
.arm
.global _start
_start:

mov r1, #Vector
mov r3, #Vector

vermayuscula:
	ldrb r2, [r1]
	cmp r2, #0
	beq imprimir
        cmp r2, #97
	subge r2, #32	
	strb r2, [r1]
	add r1, #1
b vermayuscula

imprimir:
ldrb r1, [r3]
add r3, #1
cmp r1, #0
beq loop
b imprimir

loop:
b loop


Vector: .asciz "Hola MunDo"
.balign 4
.end

/*Definición de vector:
vect1: .word 10, 20, 30, 40, 50
Equivale
vect1: .word 10
       .word 20
       .word 30
       .word 40
       .word 50
un word tiene 4bit, asi que si le sumo a la direccion #4, paso al siguiente word
vect1--> apunta a la primera posicion (10)
       */

.text
.arm
.global _start
_start:

ldr r2, =vect1
/*Opcion 1*/
ldr r1, [r2]
add r2, #4
ldr r1, [r2]

/*Opcion 2
ldr r1, [r2, #4] */

/*Opcion 3
ldr r1, [r2], #4 
va a leer la posicion de r2 y lugeo que lo cargo en r1, le suma a r2+4 */

   1 .text
   2 .arm
   3 .global _start
   4 _start:
   5         b reset
   6         b loop
   7         b loop
   8         b loop
   9         b loop
  10         nop
  11         b loop
  12         b loop
  13 
  14 /* ==============================
  15 *  TEST CODE
  16 *  ==============================
  17 */
  18 reset:
  19         mov r1,#0
  20         mov r4,#0
  21         mov r2,#VECT
  22 	
  23 
  24 otro: 	ldrb r3,[r2]
  25 	cmp r3,#0 
  26         cmpne r3,#' '
  27         beq espacio 
  28 
  29 	cmp r3,#'a'
  30 	addlo r3,r3,#('a'-'A') 
  31         strb r3,[r2]
  32         add r4,r4,#1
  33 
  34 seguir: add r2,r2,#1
  35         b otro
  36 
  37 espacio:
  38         cmp r4,#3
  39         blo seguir_esp
  40         ldrb r5,[r2,-r4]
  41         sub r5,r5,#('a'-'A')
  42         strb r5,[r2,-r4]
  43 seguir_esp:
  44         mov r4,#0                     
  45         cmp r3,#0
  46 	bne seguir
  47 
  48 salir:
  49 
  50 loop:   b loop
  51 
  52 /* ==============================
  53 *  CONTANTES
  54 *  ==============================
  55 */
  56 VECT:	.asciz "oraCiOn PAra vER sI fUNCIonA"
  57         .balign 4
  58 
  59         .end

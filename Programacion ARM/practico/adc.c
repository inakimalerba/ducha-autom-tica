#include "adc.h"
unsigned int salida;

void iniciar_adc() 
{
	unsigned int configurar,aux; //generamos 32 bits
	configurar=0;
	salida=0;
	aux = 1; //selecionamos el ADC0
	configurar|=aux; 
	aux=(16<<8); //seleccionamos el clk por division en 16
	configurar|=aux; 
	aux=(1<<21); //ponemos el adc como activo
	configurar|=aux;
	AD_ADCR=configurar; //pasamos todo a configuracion 
VICVectCntl0=(0x20|18); 	// fuente de interrupción del adc _ bit del 4:0
VICVectAddr0=(unsigned)interrupcion; // asignar el puntero de la función que atiende la interrupción
VICIntSelect&=~(1 << 18); // habilitar la interrupción del adc
VICIntEnable|=(1<<18); // habilitar la interrupción del adc
AD_ADCR|=(1<<24);
}

void interrupcion(void)
{salida=((AD_ADDR>>6)&0x03FF); /* tomar el resultado del bit 6 a 15*/
 VICVectAddr=0x00000000;
 AD_ADCR|=(1<<24);
}
	
unsigned int convertir()
{
	return salida;
}
	
	



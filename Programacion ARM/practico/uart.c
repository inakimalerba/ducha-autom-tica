#include "uart.h"
#include "lpc2114.h"
/* ----------------------------------------------------------------------------
 * inicializar UART
 * ----------------------------------------------------------------------------*/
void UART_Init()
{
   PCB_PINSEL0 |= 0x00000005;     
   UART0_IER = 0x00;             // Deshabilita todas las interrupciones
   UART0_IIR = 0x00;             // Borrar identificaciones de interrupciones
   UART0_LSR = 0x00;             // Borra el "line status register"
   UART0_RBR = 0x00;             // Borra el "receive register"
 }
/* ----------------------------------------------------------------------------
 * inicializar Baud Rate
 * ----------------------------------------------------------------------------*/
void UART_BaudRateConfig(unsigned int BaudRate)
{
   UART0_LCR |= (1<<7);               //DLAB en 1;
   UART0_DLL = (unsigned char) (BaudRate >> 0);
   UART0_DLM = (unsigned char) (BaudRate >> 8);
   UART0_LCR &= ~(1<<7);              //DLAB en 0;
}
/* ----------------------------------------------------------------------------
 * enviar un byte
 * ----------------------------------------------------------------------------*/
void UART_ByteSend(unsigned char *Data)
{
  if (((UART0_FCR >> 0) & 1) == 1)          //Si la FIFO está habilitada.
  {
    while(((UART0_LSR >> 5) & 1) == 0);    //Esperar hasta que al menos 1 posición de la FIFO esté libre.
  }
  else                                      //Si la FIFO no está habilitada.
  {
    while(((UART0_LSR >> 6) & 1) == 0);    //Esperar hasta que el shift register del transmisor esté vacío.
  }
  UART0_THR = *Data;
}
/* ----------------------------------------------------------------------------
 * enviar cadena
 * ----------------------------------------------------------------------------*/
void UART_StringSend(unsigned char *data)
{
  int i;
  unsigned char enter = 13;
  for (i = 0;;i++) {
    if(data[i] == 0) {
      UART_ByteSend(&enter);
      break;
    } else {
      UART_ByteSend(&data[i]);
    }
  }    
}





#ifndef UART_H
#define UART_H
#include "lpc2114.h"
//-------------------Oscilador--------------
//
#define FOSC		14745600         //Frecuencia del cristal en Hz
#define PLL_M	  	4                //Multiplicador del PLL
#define VPBDIV_VAL	4                //Divisor
#define UART_BAUD(Baud)  (unsigned int)(((FOSC * PLL_M /  VPBDIV_VAL) / ((Baud) * 16.0)) + 0.5)

void UART_Init();
void UART_BaudRateConfig(unsigned int BaudRate);
void UART_ByteSend(unsigned char *Data);
void UART_StringSend(unsigned char *data);
#endif

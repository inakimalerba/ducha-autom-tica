#ifndef ADC_H
#define ADC_H
#include "lpc2114.h"
void iniciar_adc();
unsigned int convertir(void);
void interrupcion(void) __attribute__((interrupt("IRQ")));
#endif


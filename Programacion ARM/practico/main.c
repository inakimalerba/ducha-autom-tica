#include "main.h"
#define CONSTANTE 3.3/1024


int main (void)
{
  unsigned int adc, entero, entero2, dec1, dec2, dec3, dec4;
  float resultado, temporal, temporal2;
  unsigned char cadena[58]={"La tension medida es  .   y la tension calculada es  .  "};
  /*INICIALIZAR UART */
  adc=0;
  UART_Init();
  /* configurar el baud rate a 9600 baudios */
  UART_BaudRateConfig(UART_BAUD(9600));
  /* Configuración del registro de control */
  UART0_LCR = 0x03; /* 00000011 8 bits sin paridad y 1 bit de stop */
  /* Configuración de la FIFO */
  UART0_FCR = 0x07; /* 00000111 activar fifo, reset de fifo Tx y Rx, trigger FIFO Rx = 1 caracter */
  /*Fin de inicializacion UART*/
  iniciar_adc();

  while(1)
  {
	  adc=convertir(); //obtenemos el número de cuentas del ADC
	  resultado=adc*(CONSTANTE); //multiplicamos por el rango

	  /*Bloque para agregar la tensión del ADC a la cadena*/
	  temporal=resultado;
	  temporal2=resultado*(5/3.3);
	  entero=(unsigned int)temporal;
	  temporal=(temporal-entero)*10;
	  dec1=(unsigned int)temporal;
	  temporal=(temporal-dec1)*10;
	  dec2=(unsigned int)temporal;
	  cadena[21]=entero+48;
	  cadena[23]=dec1+48;
	  cadena[24]=dec2+48;

	  /*Bloque para agregar la tensión calculada a la cadena*/
	  entero2=(unsigned int)temporal2;
	  temporal2=(temporal2-entero2)*10;
	  dec3=(unsigned int)temporal2;
	  temporal2=(temporal2-dec3)*10;
	  dec4=(unsigned int)temporal2;
	  cadena[52]=entero2+48;
	  cadena[54]=dec3+48;
	  cadena[55]=dec4+48;
	  cadena[56]=0x0D; //retorno del carro
	  cadena[57]='\n'; //salto de linea
          UART_StringSend(&cadena[0]);//Envia el string a la uart
  }
  return 0;
 }

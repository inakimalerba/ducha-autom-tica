#ifndef PID_H
#define PID_H
#define Kp 4
#define Td 0.05
#define Ti 0.2
#define T 0.002
#define qd Kp*(Td/T)
#define qi Kp*(T/Ti)
void inic_pid(float MAXIMO, float MINIMO, float error);
float ctrl_pid(float y, float r);
volatile float u, e1, I, umax, umin;
#endif

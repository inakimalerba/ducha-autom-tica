#include "main.h"

unsigned int adc_salida(void);
void adc_init(void);
void UART_BaudRateConfig(unsigned int);
void UART_ByteSend(unsigned char *, int);
void UART_Init(void);

int main(void)
{
unsigned char carac[]="El valor es:";
unsigned char numero[3];
unsigned int cuenta;
float valor;

  /* inicializar UART */
  UART_Init();
  /* configurar el baud rate a 9600 baudios */
  UART_BaudRateConfig(UART_BAUD(9600));
  /* Configuración del registro de control */
  U0LCR |= 0x03; /* 00000011 8 bits sin paridad y 1 bit de stop */
  /* Configuración de la FIFO */
  U0FCR |= 0x07; /* 00000111 activar fifo, reset de fifo Tx y Rx, trigger FIFO Rx = 1 caracter */
  adc_init();

while(1)
{
 cuenta=adc_salida();
 valor=cuenta*0.01953125;

	if(valor<1)
		{numero[0]='0';}
	else{if(valor<2)
		{numero[0]='1';valor=valor-1;}
	     else{if(valor<3){numero[0]='2';valor=valor-2;}
		  else{if(valor<4){numero[0]='3';valor=valor-3;}
		       else{if(valor<4.98){numero[0]='4';valor=valor-4;}
			    else{numero[0]='5';}}}}}
	 

	if(valor<0.1)
		{numero[1]='0';}
	else{if(valor<0.2)
		{numero[1]='1';valor=valor-0.1;}
	     else{if(valor<0.3){numero[1]='2';valor=valor-0.2;}
		  else{if(valor<0.4){numero[1]='3';valor=valor-0.3;}
		       else{if(valor<0.5){numero[1]='4';valor=valor-0.4;}
			    else{if(valor<0.6){numero[1]='5';valor=valor-0.5;}
				 else{if(valor<0.7){numero[1]='6';valor=valor-0.6;}
				      else{if(valor<0.8){numero[1]='7';valor=valor-0.7;}
					   else{if(valor<0.9){numero[1]='8';valor=valor-0.8;}
						else{numero[1]='9';}}}}}}}}}
	
       
        if(valor<0.01)
		{numero[2]='0';}
	else{if(valor<0.02)
		{numero[2]='1';}
	     else{if(valor<0.03){numero[2]='2';}
		  else{if(valor<0.04){numero[2]='3';}
		       else{if(valor<0.05){numero[2]='4';}
			    else{if(valor<0.06){numero[2]='5';}
				 else{if(valor<0.07){numero[2]='6';}
				      else{if(valor<0.08){numero[2]='7';}
					   else{if(valor<0.09){numero[2]='8';}
						else{numero[2]='9';}}}}}}}}}
 UART_ByteSend(carac, 12);
 UART_ByteSend(numero, 3);
}
  return 0; 
}


void adc_init(void)
{
  unsigned int configura, canal;

  canal = 1;
  configura|= (16 <<8 );   /* CLKDIV = 16 configuro divisor externo  */
  configura|= (1 << 21 ); /* PDN = 1 ADC en modo operacional*/
  configura|= canal;      /* canal a realizar la conversión */

  ADCR = configura;
}  

unsigned int adc_salida(void)
{ ADCR|= (1 << 24);  /* START = 001 comenzar la conversión inmediatamente  */
 unsigned salida;
    do {
      salida = ADDR;
    } while (( salida & (1<<31) ) == 0); /* DONE = 0 termino de convertir */

    salida = (salida >>6) & 0x03FF; /* tomar el resultado del bit 6 a 15*/

  return salida; 
}


void UART_ByteSend(unsigned char *Data, int cant)
{int i;
 for(i=0; i<cant; i++)
 {
  while((U0LSR & (1 << 5)) == 0);    //Esperar hasta que al menos 1 posición de la FIFO esté libre.
  {U0THR = *Data;}
 Data++;
 }
}

void UART_BaudRateConfig(unsigned int BaudRate)
{
   U0LCR |= (1<<7);               //DLAB en 1;
   U0DLL = (unsigned char) (BaudRate >> 0);
   U0DLM = (unsigned char) (BaudRate >> 8);
   U0LCR &= ~(1<<7);              //DLAB en 0;
}

void UART_Init()
{
   PINSEL0 |= 0x00000005;     
   U0IER = 0x00;             // Deshabilita todas las interrupciones
   U0IIR = 0x00;             // Borrar identificaciones de interrupciones
   U0LSR = 0x00;             // Borra el "line status register"
   U0RBR = 0x00;             // Borra el "receive register"
 }


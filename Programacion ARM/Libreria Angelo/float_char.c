#include "float_char.h"

void float_char(float tension_medida, unsigned char *buf, unsigned int cifra_significativa)
{int aux, i;

 for(i=0; i<4; i++)
	{if(i==cifra_significativa)
	  {*buf='.'; buf++;}//Como se diseño para solo un entero pone el punto luego de una conversion(era para un ADC 5V)
	else{aux=(int)tension_medida;//Toma la parte entera y entra al case
		switch(aux)
			{case 0: *buf='0'; break;
			 case 1: *buf='1'; break;
                	 case 2: *buf='2'; break;
	 		 case 3: *buf='3'; break;
			 case 4: *buf='4'; break;
 			 case 5: *buf='5'; break;
			 case 6: *buf='6'; break;
 			 case 7: *buf='7'; break;
   			 case 8: *buf='8'; break;
  			 case 9: *buf='9'; break;}

          tension_medida=((tension_medida-aux)*10)+0.01;//Multiplica por diez y le suma el error que tiene el float
	  buf++;}//Pasa a la siguiente posicion de la cadena de char
}
*buf='\0';//Indica el fin de la cadena
}



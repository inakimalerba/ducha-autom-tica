#ifndef LCD_H
#define LCD_H
#define Tiempo_Enable 100 //Tiempo de enable activo
#define Tiempo_Dato 10000//Tiempo de inicializacion del dato
#define ENABLE 0x00000100 //Pin de abilitar
#define RS 0x00000400 //Comando de activar escritura
#define LIMPIAR 0x00000001//Comando de Limpiar Pantalla
#include "lpc2114.h"
#include "delay.h"
void inic_lcd_8bit(void);//Inicializa el lcd con un bus de dato de 8bit
void imprimir_lcd(unsigned char *);//Se encarga de imprimir una cadena
#endif

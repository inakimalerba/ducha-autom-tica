#include "lcd.h"

/*INICIALIZACION DE LCD 16x2*/
void inic_lcd_8bit()
{
	PINSEL0=0x00400000;//Defino las salidas P0.0 a P0.8 como GPIO(El 4 esta para configurar el pin del adc)LEER!
        IO0DIR=0x000007FF;//8 Bit de Datos más el bit de Enable, bit de RS y bit R/W
	//RS|RW|ENABLE|(MSB)DB7|DB6|DB5|DB4|DB3|DB2|DB1|DB0(LSB)
	IO0CLR=0x000007FF;//Pongo todas las salidas en 0
	//Pasos 30h->0Ch->01h->06h

	IO0SET=0x00000030;//Paso 1 "00110000"
	delay_us(Tiempo_Dato);//10ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=0x00000030;//Pongo en cero todos los bit anteriormente activados

	IO0SET=0x0000000C;//Paso 2 "00001100"
	delay_us(Tiempo_Dato);//10ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
 	delay_us(Tiempo_Dato);//10ms
	IO0CLR=0x0000000C;//Pongo en cero todos los bit anteriormente activados

        IO0SET=LIMPIAR;//Paso 3 "00000001"
	delay_us(Tiempo_Dato);//10 ms
	IO0CLR=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=LIMPIAR;//Pongo en cero todos los bit anteriormente activados

	IO0SET=0x00000006;//Paso 4 "00000110"
	delay_us(Tiempo_Dato);//10 ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=0x000007FF;//Pongo en cero todos los bit
}

/*ENVIAR CADENA DE CARACTERES*/
void imprimir_lcd(unsigned char *Palabra)
{

 IO0SET=LIMPIAR;//Limpio el lcd "00000001"
 delay_us(10);//10us
 IO0SET=ENABLE;//Habilito
 delay_us(30);//30us
 IO0CLR=ENABLE;//Desabilito
 delay_us(10);//10us
 IO0CLR=LIMPIAR;//Pongo en bajo el bit DB0
 IO0SET=0x0000000C;//Caracteristicas de escritura del lcd
 delay_us(2000);//2ms--> Hoja de dato aprox 1,64ms
 IO0SET=ENABLE;//Habilito
 delay_us(30);//30us
 IO0CLR=ENABLE;//Desabilito
 delay_us(10);//10us
 IO0CLR=0x0000000C;//Pongo en bajo los bit anteriores "00001100"
 IO0SET=0x00000080;//Direccion de memoria a escribir y de allí cada caracter se desplaza +1
 delay_us(10);//10us
 IO0SET=ENABLE;//Habilito
 delay_us(30);//30us
 IO0CLR=ENABLE;//Desabilito
 delay_us(100);//100us
 IO0CLR=0x00000080;//Pongo en bajo el bit

 //Comienza el proceso de escribir los caracteres de la cadena
while(*Palabra!='\0')//Mientas no se haya llegado al fin de la cadena
	{
         IO0SET=(RS|*Palabra);//Activo el pin de escribir y paso el caracter guardado en la primer posición
	 delay_us(10);//10us
 	 IO0SET=ENABLE;//Habilito
 	 delay_us(20);//20us
	 IO0CLR=ENABLE;//Desabilito
	 delay_us(50);//50us--> Hoja de dato aprox 40us
 	 IO0CLR=(RS|0x00000FF);//Pongo en bajo todos los pines
 	 Palabra++;}//Paso al siguiente caracter
}




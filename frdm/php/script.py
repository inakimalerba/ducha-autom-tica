import serial
import string
import time
import xml.etree.cElementTree as ET
def readlineCR(port):
    rv = ""
    while True:
        ch = port.read()
        rv += ch
        if ch=='\r' or ch=='':
            return rv

tObjetivo_anterior = 0
estado_anterior = "Cerrada"

port = serial.Serial("/dev/ttyACM1", baudrate=115200, timeout=3.0)
bCerrado = 99 #Cerrado
port.write(chr(bCerrado))

while True:
    try:
        time.sleep(1)

        tree = ET.parse('data.xml')
        root = tree.getroot()
        for child in root:
            for c in child:
                if(c.tag == "tObjetivo"):
                    if not c.text == tObjetivo_anterior:
                        tObjetivo_anterior = c.text

                        bSet = 115

                        port.write(chr(bSet))

                        port.write((c.text[0]))
                        port.write((c.text[1]))

                        print "Nueva temp: "+ c.text
                if(c.tag == "estado"):
                    if not c.text == estado_anterior:
                        print "Nuevo estado: "
                        if c.text == "Abierta":
                            bAbierta = 97
                            port.write(chr(bAbierta))
                            print "a"
                        else:
                            bCerrado = 99
                            port.write(chr(bCerrado))
                            print "c"
        
        time.sleep(0.1)
        port.write(chr(98))
        rcv = readlineCR(port)
        print rcv
        tF, tC, tM, tO, pF, pC, estado = string.split(rcv, '-')
        estado = string.split(estado,'\r')

        if(estado): 
            estado = "Abierta"
            estado_anterior = "Abierta"
        else: 
            estado = "Cerrada"
            estado_anterior = "Cerrada"

        root = ET.Element("root")
        doc = ET.SubElement(root, "doc")
        
        ET.SubElement(doc, "tFria", name="tFria").text = tF
        ET.SubElement(doc, "tCaliente", name="tCaliente").text = tC
        ET.SubElement(doc, "tMezcla", name="tMezcla").text = tM
        ET.SubElement(doc, "estado", name="estado").text = estado
        ET.SubElement(doc, "tObjetivo", name="tObjetivo").text = tO
        
        tree = ET.ElementTree(root)

        tree.write("data.xml")


    except:
        pass

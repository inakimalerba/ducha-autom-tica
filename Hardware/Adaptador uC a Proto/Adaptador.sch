EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X12 P4
U 1 1 5622A82E
P 6250 3650
F 0 "P4" H 6250 4300 50  0000 C CNN
F 1 "CONN_02X12" V 6250 3650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x12" H 6250 2450 60  0001 C CNN
F 3 "" H 6250 2450 60  0000 C CNN
	1    6250 3650
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X13 P3
U 1 1 5622A867
P 7850 3600
F 0 "P3" H 7850 4300 50  0000 C CNN
F 1 "CONN_02X13" V 7850 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x13" H 7850 2450 60  0001 C CNN
F 3 "" H 7850 2450 60  0000 C CNN
	1    7850 3600
	1    0    0    -1  
$EndComp
Text GLabel 5850 3100 0    60   Output ~ 0
SW1
Text GLabel 5850 3200 0    60   Output ~ 0
SW2
Text GLabel 5850 3300 0    60   Output ~ 0
SW3
Text GLabel 5850 3400 0    60   Output ~ 0
SW4
NoConn ~ 6000 3500
Text GLabel 5850 3600 0    60   Input ~ 0
ADC3
NoConn ~ 6000 3700
NoConn ~ 6000 3800
Text GLabel 5850 3900 0    60   Output ~ 0
LED1
Text GLabel 5850 4000 0    60   Output ~ 0
LED2
NoConn ~ 6000 4100
Text GLabel 5850 4200 0    60   Output ~ 0
MOSI
Text GLabel 6650 4200 2    60   Output ~ 0
SS
Text GLabel 6650 4100 2    60   Output ~ 0
MISO
Text GLabel 6650 4000 2    60   Output ~ 0
SCK
NoConn ~ 6500 3900
NoConn ~ 6500 3200
NoConn ~ 6500 3300
Text GLabel 6650 3800 2    60   Input ~ 0
RXD
Text GLabel 6650 3700 2    60   Output ~ 0
TXD
Text GLabel 6650 3600 2    60   Input ~ 0
ADC2
Text GLabel 6650 3500 2    60   Input ~ 0
ADC1
Text GLabel 6650 3400 2    60   Input ~ 0
ADC0
Text GLabel 6650 3100 2    60   BiDi ~ 0
P0.22
Wire Wire Line
	6650 3100 6500 3100
Wire Wire Line
	6650 3400 6500 3400
Wire Wire Line
	6650 3500 6500 3500
Wire Wire Line
	6650 3600 6500 3600
Wire Wire Line
	6650 3700 6500 3700
Wire Wire Line
	6650 3800 6500 3800
Wire Wire Line
	6650 4000 6500 4000
Wire Wire Line
	6650 4100 6500 4100
Wire Wire Line
	6650 4200 6500 4200
Wire Wire Line
	6000 4200 5850 4200
Wire Wire Line
	6000 4000 5850 4000
Wire Wire Line
	6000 3900 5850 3900
Wire Wire Line
	6000 3600 5850 3600
Wire Wire Line
	6000 3400 5850 3400
Wire Wire Line
	6000 3300 5850 3300
Wire Wire Line
	6000 3200 5850 3200
Wire Wire Line
	6000 3100 5850 3100
Text GLabel 7450 3000 0    60   Input ~ 0
GND
Text GLabel 7450 3100 0    60   Input ~ 0
RESET
NoConn ~ 7600 3200
NoConn ~ 7600 3300
Text GLabel 7450 3400 0    60   Output ~ 0
D1
Text GLabel 7450 3600 0    60   Output ~ 0
D3
NoConn ~ 7600 3500
Text GLabel 7450 3700 0    60   Output ~ 0
D5
Text GLabel 7450 3800 0    60   Output ~ 0
D6
Text GLabel 7450 3900 0    60   Output ~ 0
D7
Text GLabel 7450 4000 0    60   Output ~ 0
R/W
Text GLabel 7450 4100 0    60   Output ~ 0
RS
Text GLabel 7450 4200 0    60   Output ~ 0
PWM4
Text GLabel 8250 4100 2    60   Output ~ 0
PWM6
Text GLabel 8250 3900 2    60   Output ~ 0
E
Text GLabel 8250 3600 2    60   Output ~ 0
D4
Text GLabel 8250 3400 2    60   Output ~ 0
D2
Text GLabel 8250 3300 2    60   Output ~ 0
D0
Text GLabel 8250 3100 2    60   Output ~ 0
+3.3v
Text GLabel 8250 3000 2    60   Input ~ 0
+5v
Wire Wire Line
	8250 3000 8100 3000
Wire Wire Line
	8250 3100 8100 3100
Wire Wire Line
	8250 3300 8100 3300
Wire Wire Line
	8250 3400 8100 3400
Wire Wire Line
	8250 3600 8100 3600
Wire Wire Line
	8250 3900 8100 3900
Wire Wire Line
	8250 4100 8100 4100
Wire Wire Line
	7450 4200 7600 4200
Wire Wire Line
	7600 4100 7450 4100
Wire Wire Line
	7600 4000 7450 4000
Wire Wire Line
	7600 3900 7450 3900
Wire Wire Line
	7600 3800 7450 3800
Wire Wire Line
	7600 3700 7450 3700
Wire Wire Line
	7600 3600 7450 3600
Wire Wire Line
	7600 3400 7450 3400
Wire Wire Line
	7450 3000 7600 3000
Wire Wire Line
	7600 3100 7450 3100
Text GLabel 4850 2800 2    60   Input ~ 0
SW1
Text GLabel 4850 2700 2    60   Input ~ 0
SW2
Text GLabel 4850 2600 2    60   Input ~ 0
SW3
Text GLabel 4850 2500 2    60   Input ~ 0
SW4
Text GLabel 3400 3800 2    60   Output ~ 0
ADC2
Text GLabel 3400 3900 2    60   Output ~ 0
ADC1
Text GLabel 3400 4000 2    60   Output ~ 0
ADC0
Text GLabel 3400 3700 2    60   Output ~ 0
ADC3
Text GLabel 3450 2900 2    60   Input ~ 0
MOSI
Text GLabel 3450 3100 2    60   Input ~ 0
MISO
Text GLabel 3450 3200 2    60   Input ~ 0
SCK
Text GLabel 3450 3000 2    60   Input ~ 0
SS
Text GLabel 4800 4600 2    60   Output ~ 0
+5v
Text GLabel 4800 4500 2    60   Output ~ 0
GND
Text GLabel 4800 4400 2    60   Output ~ 0
RESET
Text GLabel 4800 4700 2    60   Input ~ 0
+3.3v
Wire Wire Line
	4800 4400 4650 4400
Wire Wire Line
	4800 4500 4650 4500
Wire Wire Line
	4800 4600 4650 4600
Wire Wire Line
	4800 4700 4650 4700
Text GLabel 3450 2600 2    60   Input ~ 0
LED1
Text GLabel 3450 2700 2    60   Input ~ 0
LED2
Text GLabel 3450 2500 2    60   BiDi ~ 0
P0.22
Text GLabel 3450 3500 2    60   Input ~ 0
TXD
Text GLabel 3450 3400 2    60   Output ~ 0
RXD
Text GLabel 3450 4700 2    60   Input ~ 0
PWM6
Text GLabel 3450 4600 2    60   Input ~ 0
PWM4
Text GLabel 3450 4300 2    60   Output ~ 0
RS
Text GLabel 3450 4200 2    60   Output ~ 0
R/W
Text GLabel 3450 4400 2    60   Output ~ 0
E
Text GLabel 4850 4000 2    60   Output ~ 0
D0
Text GLabel 4850 3900 2    60   Output ~ 0
D1
Text GLabel 4850 3800 2    60   Output ~ 0
D2
Text GLabel 4850 3700 2    60   Output ~ 0
D3
Text GLabel 4850 3600 2    60   Output ~ 0
D4
Text GLabel 4850 3500 2    60   Output ~ 0
D5
Text GLabel 4850 3400 2    60   Output ~ 0
D6
Text GLabel 4850 3300 2    60   Output ~ 0
D7
NoConn ~ 8100 3200
NoConn ~ 8100 3500
NoConn ~ 8100 3700
NoConn ~ 8100 3800
NoConn ~ 8100 4000
NoConn ~ 8100 4200
Wire Wire Line
	3450 2500 3250 2500
Wire Wire Line
	3450 2600 3250 2600
Wire Wire Line
	3450 2700 3250 2700
Wire Wire Line
	3450 2900 3250 2900
Wire Wire Line
	3450 3000 3250 3000
Wire Wire Line
	3450 3100 3250 3100
Wire Wire Line
	3450 3200 3250 3200
Wire Wire Line
	3450 3400 3250 3400
Wire Wire Line
	3450 3500 3250 3500
Wire Wire Line
	3450 4200 3250 4200
Wire Wire Line
	3450 4300 3250 4300
Wire Wire Line
	3450 4400 3250 4400
Wire Wire Line
	3450 4600 3250 4600
Wire Wire Line
	3450 4700 3250 4700
NoConn ~ 3250 4500
NoConn ~ 3250 4100
NoConn ~ 3250 3300
NoConn ~ 3250 2800
Wire Wire Line
	4850 2500 4650 2500
Wire Wire Line
	4850 2600 4650 2600
Wire Wire Line
	4850 2700 4650 2700
Wire Wire Line
	4850 2800 4650 2800
Wire Wire Line
	4850 3300 4650 3300
Wire Wire Line
	4850 3400 4650 3400
Wire Wire Line
	4850 3500 4650 3500
Wire Wire Line
	4850 3600 4650 3600
Wire Wire Line
	4850 3700 4650 3700
Wire Wire Line
	4850 3800 4650 3800
Wire Wire Line
	4850 3900 4650 3900
Wire Wire Line
	4850 4000 4650 4000
NoConn ~ 4650 2900
NoConn ~ 4650 4300
$Comp
L CONN_01X23 P2
U 1 1 5623053D
P 4450 3600
F 0 "P2" H 4450 4800 50  0000 C CNN
F 1 "CONN_01X23" V 4550 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x23" H 4450 3600 60  0001 C CNN
F 3 "" H 4450 3600 60  0000 C CNN
	1    4450 3600
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X23 P1
U 1 1 56231A9A
P 3050 3600
F 0 "P1" H 3050 4800 50  0000 C CNN
F 1 "CONN_01X23" V 3150 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x23" H 3050 3600 60  0001 C CNN
F 3 "" H 3050 3600 60  0000 C CNN
	1    3050 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3400 3700 3250 3700
Wire Wire Line
	3400 3800 3250 3800
Wire Wire Line
	3400 3900 3250 3900
Wire Wire Line
	3400 4000 3250 4000
NoConn ~ 3250 3600
NoConn ~ 4650 3000
NoConn ~ 4650 3100
NoConn ~ 4650 3200
NoConn ~ 4650 4100
NoConn ~ 4650 4200
$EndSCHEMATC

EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LCD-Teclado-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SW_PUSH SW1
U 1 1 561E9832
P 6150 5050
F 0 "SW1" H 6300 5160 50  0000 C CNN
F 1 "SW_PUSH" H 6150 4970 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_PUSH_SMALL" H 6150 5050 60  0001 C CNN
F 3 "" H 6150 5050 60  0000 C CNN
	1    6150 5050
	0    1    1    0   
$EndComp
$Comp
L SW_PUSH SW2
U 1 1 561E987B
P 6950 5050
F 0 "SW2" H 7100 5160 50  0000 C CNN
F 1 "SW_PUSH" H 6950 4970 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_PUSH_SMALL" H 6950 5050 60  0001 C CNN
F 3 "" H 6950 5050 60  0000 C CNN
	1    6950 5050
	0    1    1    0   
$EndComp
$Comp
L SW_PUSH SW3
U 1 1 561E98E8
P 7750 5050
F 0 "SW3" H 7900 5160 50  0000 C CNN
F 1 "SW_PUSH" H 7750 4950 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_PUSH_SMALL" H 7750 5050 60  0001 C CNN
F 3 "" H 7750 5050 60  0000 C CNN
	1    7750 5050
	0    1    1    0   
$EndComp
$Comp
L SW_PUSH SW4
U 1 1 561E9931
P 8550 5050
F 0 "SW4" H 8700 5160 50  0000 C CNN
F 1 "SW_PUSH" H 8550 4970 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_PUSH_SMALL" H 8550 5050 60  0001 C CNN
F 3 "" H 8550 5050 60  0000 C CNN
	1    8550 5050
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 561E9982
P 5850 4650
F 0 "R1" V 5750 4650 50  0000 C CNN
F 1 "330" V 5950 4650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5780 4650 30  0001 C CNN
F 3 "" H 5850 4650 30  0000 C CNN
	1    5850 4650
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 561E99E3
P 6150 5600
F 0 "R2" V 6230 5600 50  0000 C CNN
F 1 "10K" V 6050 5600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 6080 5600 30  0001 C CNN
F 3 "" H 6150 5600 30  0000 C CNN
	1    6150 5600
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 561E9A76
P 6950 5600
F 0 "R3" V 7030 5600 50  0000 C CNN
F 1 "10K" V 6850 5600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 6880 5600 30  0001 C CNN
F 3 "" H 6950 5600 30  0000 C CNN
	1    6950 5600
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 561E9A9B
P 7750 5600
F 0 "R4" V 7850 5600 50  0000 C CNN
F 1 "10K" V 7650 5600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7680 5600 30  0001 C CNN
F 3 "" H 7750 5600 30  0000 C CNN
	1    7750 5600
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 561E9ADC
P 8550 5600
F 0 "R5" V 8630 5600 50  0000 C CNN
F 1 "10K" V 8450 5600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 8480 5600 30  0001 C CNN
F 3 "" H 8550 5600 30  0000 C CNN
	1    8550 5600
	1    0    0    -1  
$EndComp
$Comp
L POT RV1
U 1 1 561E9B2D
P 4900 3100
F 0 "RV1" H 4900 3000 50  0000 C CNN
F 1 "1K" H 5100 3200 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 4900 3100 60  0001 C CNN
F 3 "" H 4900 3100 60  0000 C CNN
	1    4900 3100
	0    1    1    0   
$EndComp
NoConn ~ 4500 4550
Text Label 4500 4850 0    60   ~ 0
GND
Text Label 4500 4950 0    60   ~ 0
+5V
Text Label 4500 4550 0    60   ~ 0
NC
Text Label 4500 4450 0    60   ~ 0
RS
Text Label 4500 4350 0    60   ~ 0
R/W
Text Label 4500 4250 0    60   ~ 0
E
Text Label 4500 4150 0    60   ~ 0
D0
Text Label 4500 4050 0    60   ~ 0
D1
Text Label 4500 3950 0    60   ~ 0
D2
Text Label 4500 3850 0    60   ~ 0
D3
Text Label 4500 3750 0    60   ~ 0
D4
Text Label 4500 3650 0    60   ~ 0
D5
Text Label 4500 3550 0    60   ~ 0
D6
Text Label 4500 3450 0    60   ~ 0
D7
Text Label 4500 4650 0    60   ~ 0
SW1
Text Label 4500 4750 0    60   ~ 0
SW2
Text Label 4500 5050 0    60   ~ 0
SW3
Text Label 4500 5150 0    60   ~ 0
SW4
$Comp
L POT RV2
U 1 1 5620F8FB
P 6550 3100
F 0 "RV2" H 6550 3000 50  0000 C CNN
F 1 "1K" H 6750 3200 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Triwood_RM-065" H 6550 3100 60  0001 C CNN
F 3 "" H 6550 3100 60  0000 C CNN
	1    6550 3100
	0    -1   1    0   
$EndComp
$Comp
L C C4
U 1 1 5633A040
P 8250 5600
F 0 "C4" H 8275 5700 50  0000 L CNN
F 1 "0.1uF" H 8275 5500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 8288 5450 30  0001 C CNN
F 3 "" H 8250 5600 60  0000 C CNN
	1    8250 5600
	-1   0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5633A0B9
P 7450 5600
F 0 "C3" H 7475 5700 50  0000 L CNN
F 1 "0.1uF" H 7475 5500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 7488 5450 30  0001 C CNN
F 3 "" H 7450 5600 60  0000 C CNN
	1    7450 5600
	-1   0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5633A102
P 6650 5600
F 0 "C2" H 6675 5700 50  0000 L CNN
F 1 "0.1uF" H 6675 5500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 6688 5450 30  0001 C CNN
F 3 "" H 6650 5600 60  0000 C CNN
	1    6650 5600
	-1   0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5633A14B
P 5850 5600
F 0 "C1" H 5700 5700 50  0000 L CNN
F 1 "0.1uF" H 5600 5500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 5888 5450 30  0001 C CNN
F 3 "" H 5850 5600 60  0000 C CNN
	1    5850 5600
	1    0    0    -1  
$EndComp
Text GLabel 5650 5400 0    60   Input ~ 0
SW1
Text GLabel 6550 5400 0    60   Input ~ 0
SW2
Text GLabel 7350 5400 0    60   Input ~ 0
SW3
Wire Wire Line
	5100 2600 5100 3100
Wire Wire Line
	5100 3100 5050 3100
Wire Wire Line
	4900 2850 4900 2800
Wire Wire Line
	5000 2800 5000 2600
Wire Wire Line
	4900 3350 4900 3400
Wire Wire Line
	4900 2700 4900 2600
Wire Wire Line
	6300 2600 6300 3100
Wire Wire Line
	6300 3100 6400 3100
Wire Wire Line
	6550 2800 6550 2850
Connection ~ 5000 2800
Wire Wire Line
	6550 3400 6550 3350
Connection ~ 4900 3400
Wire Wire Line
	6400 2600 6400 2700
Wire Wire Line
	6400 2700 6700 2700
Wire Wire Line
	6700 2700 6700 3400
Connection ~ 6550 3400
Wire Wire Line
	4500 3450 6200 3450
Wire Wire Line
	6200 3450 6200 2600
Wire Wire Line
	4500 3550 6100 3550
Wire Wire Line
	6100 3550 6100 2600
Wire Wire Line
	4500 3650 6000 3650
Wire Wire Line
	6000 3650 6000 2600
Wire Wire Line
	4500 3750 5900 3750
Wire Wire Line
	5900 3750 5900 2600
Wire Wire Line
	4500 3850 5800 3850
Wire Wire Line
	5800 3850 5800 2600
Wire Wire Line
	4500 3950 5700 3950
Wire Wire Line
	5700 3950 5700 2600
Wire Wire Line
	4500 4050 5600 4050
Wire Wire Line
	5600 4050 5600 2600
Wire Wire Line
	4500 4150 5500 4150
Wire Wire Line
	5500 4150 5500 2600
Wire Wire Line
	4500 4250 5400 4250
Wire Wire Line
	5400 4250 5400 2600
Wire Wire Line
	4500 4350 5300 4350
Wire Wire Line
	5300 4350 5300 2600
Wire Wire Line
	4500 4450 5200 4450
Wire Wire Line
	5200 4450 5200 2600
Wire Wire Line
	6700 3400 4700 3400
Connection ~ 4700 3400
Wire Wire Line
	4700 2700 4900 2700
Wire Wire Line
	6000 4650 8550 4650
Wire Wire Line
	6150 4650 6150 4750
Wire Wire Line
	6150 5350 6150 5450
Connection ~ 6150 5400
Wire Wire Line
	5850 5400 5850 5450
Connection ~ 5850 5400
Wire Wire Line
	5650 5400 6150 5400
Wire Wire Line
	6950 5350 6950 5450
Wire Wire Line
	6950 5400 6550 5400
Connection ~ 6950 5400
Wire Wire Line
	6650 5450 6650 5400
Connection ~ 6650 5400
Wire Wire Line
	6950 5750 6950 5800
Wire Wire Line
	5250 5800 8550 5800
Wire Wire Line
	5850 5800 5850 5750
Wire Wire Line
	6150 5800 6150 5750
Connection ~ 6150 5800
Wire Wire Line
	6650 5750 6650 5800
Connection ~ 6650 5800
Wire Wire Line
	7750 5350 7750 5450
Wire Wire Line
	7750 5400 7350 5400
Connection ~ 7750 5400
Wire Wire Line
	7450 5450 7450 5400
Connection ~ 7450 5400
Wire Wire Line
	7750 5800 7750 5750
Connection ~ 6950 5800
Wire Wire Line
	7450 5750 7450 5800
Connection ~ 7450 5800
Text GLabel 8150 5400 0    60   Input ~ 0
SW4
Wire Wire Line
	8150 5400 8550 5400
Wire Wire Line
	8250 5400 8250 5450
Wire Wire Line
	8550 5350 8550 5450
Connection ~ 8250 5400
Connection ~ 8550 5400
Wire Wire Line
	8550 5800 8550 5750
Connection ~ 7750 5800
Wire Wire Line
	8250 5750 8250 5800
Connection ~ 8250 5800
Wire Wire Line
	8550 4650 8550 4750
Connection ~ 6150 4650
Wire Wire Line
	6950 4750 6950 4650
Connection ~ 6950 4650
Wire Wire Line
	7750 4750 7750 4650
Connection ~ 7750 4650
Connection ~ 5850 5800
Text GLabel 4750 4650 2    60   Output ~ 0
SW1
Wire Wire Line
	4750 4650 4500 4650
Text GLabel 4750 4750 2    60   Output ~ 0
SW2
Text GLabel 4750 5050 2    60   Output ~ 0
SW3
Text GLabel 4750 5150 2    60   Output ~ 0
SW4
Wire Wire Line
	4750 5150 4500 5150
Wire Wire Line
	4750 5050 4500 5050
Wire Wire Line
	4750 4750 4500 4750
$Comp
L CONN_01X11 P1
U 1 1 5634DBE6
P 4300 3950
F 0 "P1" V 4400 4450 50  0000 C CNN
F 1 "LCD" V 4400 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x11" H 4300 3950 60  0001 C CNN
F 3 "" H 4300 3950 60  0000 C CNN
	1    4300 3950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X07 P2
U 1 1 5634DC41
P 4300 4850
F 0 "P2" V 4400 4550 50  0000 C CNN
F 1 "TECLADO" V 4400 4850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07" H 4300 4850 60  0001 C CNN
F 3 "" H 4300 4850 60  0000 C CNN
	1    4300 4850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 4950 4500 4950
Wire Wire Line
	5100 4500 5100 4950
Wire Wire Line
	5100 4650 5700 4650
Wire Wire Line
	4500 4850 5250 4850
Wire Wire Line
	5250 4850 5250 5800
Wire Wire Line
	4700 2700 4700 4850
Connection ~ 4700 4850
Wire Wire Line
	5100 4500 6800 4500
Wire Wire Line
	6800 4500 6800 2800
Wire Wire Line
	6800 2800 4900 2800
Connection ~ 5100 4650
Connection ~ 6550 2800
$Comp
L LCD16X2 DS1
U 1 1 561E97F7
P 5650 2100
F 0 "DS1" H 4850 2500 40  0000 C CNN
F 1 "LCD16X2" H 6350 2500 40  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x16" H 5650 2050 35  0000 C CIN
F 3 "" H 5650 2100 60  0000 C CNN
	1    5650 2100
	1    0    0    -1  
$EndComp
$EndSCHEMATC

#ifndef MENULCD_H
#define MENULCD_H
/*ARCHIVO LISTO*/
#define MASC_BOTONES 0x3800000
#define Shift_IZQ 23
#define Shift_DER 24
#define Shift_OK 25
#define base 7
#include "lpc2114.h"
#include "main.h"
unsigned int menulcd(void);
unsigned int ventana1(void);
unsigned int ventana_confirmar(void);
unsigned int ventana_psl(void);
void modificar_param(unsigned int);
extern unsigned int param_psl[7];//Variable perteneciente a main
#endif

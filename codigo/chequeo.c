#include "chequeo.h"
/*ARCHIVO LISTO*/
unsigned int chequeo_gral(void)
{unsigned int cuenta1, aux, aux1, aux2;
 float temp1, temp2, temp_aux;
 unsigned char valor[]="INT:    Celsius";
 unsigned char valor1[]="AMB:   Celsius";
 unsigned char pot[]="Potencia: OK";
 unsigned char sen[]="Sensores: OK";
 unsigned char pru[]="Check Potencia";
 pwm_init();//Inicializo el PWM

	temp1=muestreo();//La transformo a temperatura
	cuenta1=adc_salida(3);//Pido la cuenta del adc canal 3 (LM35)
        temp2=(cuenta1*CONSTANTE_CONVERSION_LM35);//La transformo a temperatura
	//Conversion a unsigned char para imprimir en LCD
		  aux2=(unsigned int)temp1/100;
		  aux=(unsigned int)temp1;
		  aux1=(unsigned int)temp1/10;
		  valor[4]=aux2+48;
		  if(valor[4]=='0')
			{valor[4]=' ';}
		  valor[5]=48+(aux1-(aux2*10));
	          valor[6]=48+(aux-(aux1*10));
        imprimir_lcd(valor, 1);//Imprimo la informacion
	//Conversion a unsigned char para imprimir en LCD
 		  aux=(unsigned int)temp2;
		  aux1=(unsigned int)temp2/10;
		  valor1[4]=aux1+48;
			if(valor1[4]=='0')
				{valor1[4]=' ';}
                  valor1[5]=48+(aux-(aux1*10));
	imprimir_lcd(valor1, 2);//Imprimo la información
	delay_ms(2000);//Espero 2 segundos

	imprimir_lcd(sen, 1);//Imprimo sensor ok
        imprimir_lcd(pru, 2);//Imprimo prueba del sensor

	if((TMP_INF>temp1) || (TMP_SUP<temp1))//Si es menor o mayor al intervalo es porque mide ruido
		{return 1;}//Informa que el sensor esta desconectado o tiene un defecto

	else{
	if((TMP_INF1>temp2) || (TMP_SUP1<temp2))//Si es menor o mayor al intervalo es porque mide ruido
		{return 1;}//Informa que el sensor esta desconectado o tiene un defecto
	else{
	     pwm_on();
	     pwm_modul(75);
	     delay_ms(TIEMPO_ESPERA);//Espero un rato para que aumente la temperatura
	     pwm_off();
	     temp_aux=temp1+TMP_SUMA;
	     temp1=muestreo();
	   if(temp1<=temp_aux)//Si es menor a la temperatura es debido a que la etapa de potencia no funciona bien
		{return 2;}
}}

imprimir_lcd(sen, 1);
imprimir_lcd(pot, 2);
delay_ms(2000);

return 0;
}

#ifndef LCD_H
#define LCD_H
/*ARCHIVO LISTO*/
#define Tiempo_Enable 100 //Tiempo de enable activo
#define Tiempo_Dato 10000//Tiempo de inicializacion del dato
//RS|RW|ENABLE|(MSB)DB7|DB6|DB5|DB4|DB3|DB2|DB1|DB0(LSB)
#define ENABLE 0x100//Pin de abilitar
#define RS 0x400//Comando de activar escritura
#define LIMPIAR 0x01//Comando de Limpiar Pantalla
#define FUNCION_SET 0x38/*FUCTION SET|Paso 1 "00111000"('1', '1' Bus de 8bit, '1' 2 lineas)|0x38*/
#define DISPLAY_CONTROL 0x0C/*DISPLAY ON/OFF CONTROL Paso 2 "00001100"('1', '1' prendido, '0' sin cursor visible, '0' blink off)| 0x0C*/
#define MODO_SET 0x06/*ENTRY MODO SET Paso 4 "00000110"('1','1' cursor incrementa, '0'desactivado el shift de la pantalla)| 0x06*/
#define PINES_LCD 0x7FF
#define DIRECCION_LINEA_2 0xC0//0xC0
#include "lpc2114.h"
#include "delay.h"
void inic_lcd_8bit(void);//Inicializa el lcd con un bus de dato de 8bit
void imprimir_lcd(unsigned char *, unsigned int);//Se encarga de imprimir una cadena
void limpiar(void);
#endif

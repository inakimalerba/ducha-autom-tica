#ifndef CHEQUEO_H
#define CHEQUEO_H
/*ARCHIVO NO AJUSTADO*/
#define CONSTANTE_CONVERSION_LM35 0.087
#define TIEMPO_ESPERA 30000//Tiempo de espera para el aumento de temperatura
#define TMP_INF 15//Temperatura minima para verificar que no se este midiendo
#define TMP_SUP 80//Temperatura máxima para verificar que no se este midiendo
#define TMP_INF1 15//Temperatura minima para verificar que no se este midiendo
#define TMP_SUP1 80//Temperatura máxima para verificar que no se este midiendo
#define TMP_SUMA 2//La temperatura aprox. de la que debe aumentar en ese tiempo el horno
#include "adc.h"
#include "muestreo.h"
#include "pwm.h"
#include "delay.h"
#include "lcd.h"
unsigned int chequeo_gral(void);//Devuelve si cumple las caracteristicas el sensor la potencia o no.
#endif

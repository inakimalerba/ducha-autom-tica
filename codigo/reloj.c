#include "reloj.h"
/*ARCHIVO LISTO*/
void intreloj(void) __attribute__((interrupt("IRQ")));//Defino rutina cuando se genere una interrupcion en el timer0

void inic_reloj(void)
{T1TCR=0;//Contador apagado
 T1TC=0;//Pongo en 0 el Timer Counter
 T1PC=0;//Pongo en 0 Prescale Counter
 T1MCR=1;//Habilito la interrupcion del MR0
 inic_int1();//Funcion de inicializar todo lo referido a la interrupcion
}

void inic_int1(void)
{VICVectCntl2=(0x20|5);//Asigno el vector 1 a la interrupcion del timer0
 VICVectAddr2=(unsigned)intreloj;//Defino la direccion a la que debe saltar el uC al suceder la interrupcion
 VICIntEnable=(1<<5);//Habilito la interrupcion del timer0
 VICIntSelect&=~(1<<5);//La defino como tipo IRQ
}

void reloj(void)
{T1MR0=1000;//Valor de espera en ms
 segundos=0;//Inicializa los segundos
 T1PR=14700;// 14700 Prescale Register para que TC se incremente cada 1ms(CLOCK Micro 14.7456MHz)
 T1TCR=1;//Arranca el timer1
}

void parar_reloj(void)
{T1TCR=0;}

void intreloj(void)
{segundos++;
 T1TC=0;
 VICVectAddr=0x00000000;//Finalizo el IRQ, para que no siga viendo el micro las demas interrupciones vectoriales
 T1IR=1;
}


#ifndef UART_H
#define UART_H
//#include "stdio.h"
#include "lpc2114.h"
#define FOSC            14745600         //Frecuencia del cristal en Hz
#define PLL_M           4                //Multiplicador del PLL
#define VPBDIV_VAL      4                //Divisor
#define UART_BAUD(Baud)  (unsigned int)(((FOSC * PLL_M /  VPBDIV_VAL) / ((Baud) * 16.0)) + 0.5)
void UART_BaudRateConfig(unsigned int);
void UART_ByteSend(unsigned char *);
void UART_StringSend(unsigned char *);
void UART_Init(void);
#endif

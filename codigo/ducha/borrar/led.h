#ifndef LED_H
#define LED_H
/*ARCHIVO LISTO*/
#define LED_AMARILLO_ON 0
#define LED_AMARILLO_OFF 1
#define LED_VERDE_ON 2
#define LED_VERDE_OFF 3
#define LED_ROJO_ON 4
#define LED_ROJO_OFF 5
#define VERDE (1<<11)
#define AMARILLO (1<<12)
#define ROJO (1<<13)
#include "lpc2114.h"
void info_led(unsigned int);//Prende y apaga los led correspondientes
#endif

#ifndef PWM_H
#define PWM_H
#define PLOCK 0x00000400
#include "lpc2114.h"
#define PWMPRESCALE 15   //60 PCLK cycles to increment TC by 1 i.e 1 Micro-second 
#define PERIOD 20000 

void pwm_modul(int, float, float);
void pwm_init(void);
#endif

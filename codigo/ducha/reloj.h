#ifndef RELOJ_H
#define RELOJ_H
/*ARCHIVO LISTO*/
#include "lpc2114.h"
void inic_reloj(void);//Inicializa el reloj con su correspondiente PR
void reloj(void);//Arranca el reloj
void parar_reloj(void);//Para el reloj
void inic_int1(void);//Inicializa todas las interrupciones
unsigned int segundos;//Es la variable global para los segundos del reloj
#endif

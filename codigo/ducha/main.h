#ifndef MAIN_H
#define MAIN_H
/*FALTA AJUSTAR VALORES DE CADA ETAPA*/
#include "lpc2114.h"
#include "lcd.h"
#include "delay.h"
#include "adc.h"
//#include "led.h"
//#include "error.h"
//#include "chequeo.h"
//#include "menulcd.h"
#include "pwm.h"
//#include "control.h"
//#include "reloj.h"
#include "barra_medidora.h"
#include "sensado.h"
#include "uart.h"
#include "float_char.h"
#define Temperatura_A_std 110
#define Temperatura_B_std 140
#define Temperatura_C_std 230
#define Temperatura_Seguridad_std 80
#define Tiempo_A_std 120
#define Tiempo_B_std 50
#define Tiempo_C_std 90
#define Conf_Pines0 0x0000000A
#define Conf_Pines1 0x04400400
unsigned int param_psl[7], param_std[7];//Parametros personalizados modificados en menu
float temp_max, temp_min, temp_actual;
#endif

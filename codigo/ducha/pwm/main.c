/*
(C) Umang Gajera | Power_user_EX - www.ocfreaks.com 2011-13.
More Embedded tutorials @ www.ocfreaks.com/cat/embedded

LPC2148 PWM Tutorial Example 1 - RC Servo Control using PWM.
License : GPL.
*/
#include "main.h"

#define PLOCK 0x00000400
#define PWMPRESCALE 60   //60 PCLK cycles to increment TC by 1 i.e 1 Micro-second 

void initPWM(void);

int main(void)
{
    initPWM(); //Initialize PWM

    //IO0DIR = 0x1; This is not needed!
    //Also by default all pins are configured as Inputs after MCU Reset.
   
    int a = 0;
    while(1)
    {   
            a = 16000;
            PWMMR1 = a;
            PWMLER = (1<<1);
    }
    //return 0; //normally this wont execute ever
}

void initPWM(void)
{
    /*Assuming that PLL0 has been setup with CCLK = 60Mhz and PCLK also = 60Mhz.*/
    /*This is a per the Setup & Init Sequence given in the tutorial*/

    PINSEL0 = (1<<1); // Select PWM1 output for Pin0.0
    PWMPCR = 0x0; //Select Single Edge PWM - by default its single Edged so this line can be removed
    PWMPR = PWMPRESCALE-1; // 1 micro-second resolution
    PWMMR0 = 20000; // 20ms = 20k us - period duration
    PWMMR1 = 1000; // 1ms - pulse duration i.e width
    PWMMCR = (1<<1); // Reset PWMTC on PWMMR0 match
    PWMLER = (1<<1) | (1<<0); // update MR0 and MR1
    PWMPCR = (1<<9); // enable PWM output
    PWMTCR = (1<<1) ; //Reset PWM TC & PR

    //Now , the final moment - enable everything
    PWMTCR = (1<<0) | (1<<3); // enable counters and PWM Mode

    //PWM Generation goes active now!!
    //Now you can get the PWM output at Pin P0.0!
}

#ifndef ADC_H
#define ADC_H

#include "lpc2114.h"
#define Divisor_Clock_ADC (16<<8)//Velocidad de conversion (14.14MHz/16)
#define Modo_ADC_Operacional (1<<21)//Que este en modo operacional
#define Modo_ADC_Low ~(1<<21)//Esta en modo low

unsigned int adc_salida(unsigned int);//Devuelve la cuenta del ADC

#endif

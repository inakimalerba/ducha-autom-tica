#include "adc.h"
unsigned int adc_salida(unsigned int canal){
    unsigned int salida;

    ADCR = (Divisor_Clock_ADC|Modo_ADC_Operacional|canal);//Lo enciendo y elijo el canal
    ADCR |= (1<<24);

    do{
        salida = ADDR;
        }
    while( (salida&(1<<31) ) == 0 );//No sale hasta terminar la conversion

    salida = ((salida>>6) & 0x03FF);

    //ADCR &= Modo_ADC_Low;//Pongo en modo low al adc
    return salida;
}//Devuelvo la cuenta de la conversion

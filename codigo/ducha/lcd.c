#include "lcd.h"
/*ARCHIVO LISTO*/
/*INICIALIZACION DE LCD 16x2*/
void inic_lcd_8bit(void)
{
	//PINSEL0=0x00400000;//Defino las salidas P0.0 a P0.8 como GPIO(El 4 esta para configurar el pin del adc)LEER!
    //IO0DIR=PINES_LCD;//8 Bit de Datos más el bit de Enable, bit de RS y bit R/W
	IO0CLR=PINES_LCD;//Pongo todas las salidas en 0
	//Pasos 38h->0Ch->01h->06h
/*FUCTION SET*/
	IO0SET=FUNCION_SET;//Paso 1 "00111000"('1', '1' Bus de 8bit, '1' 2 lineas)
	delay_us(Tiempo_Dato);//10ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=FUNCION_SET;//Pongo en cero todos los bit anteriormente activados
/*DISPLAY ON/OFF CONTROL*/
	IO0SET=DISPLAY_CONTROL;//Paso 2 "00001100"('1', '1' prendido, '0' sin cursor visible, '0' blink off)
	delay_us(Tiempo_Dato);//10ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
 	delay_us(Tiempo_Dato);//10ms
	IO0CLR=DISPLAY_CONTROL;//Pongo en cero todos los bit anteriormente activados

    IO0SET=LIMPIAR;//Paso 3 "00000001"
	delay_us(Tiempo_Dato);//10 ms
	IO0CLR=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=LIMPIAR;//Pongo en cero todos los bit anteriormente activados
/*ENTRY MODO SET*/
	IO0SET=MODO_SET;//Paso 4 "00000110"('1','1' cursor incrementa, '0'desactivado el shift de la pantalla )
	delay_us(Tiempo_Dato);//10 ms
	IO0SET=ENABLE;//Habilito
	delay_us(Tiempo_Enable);//100us
	IO0CLR=ENABLE;//Desabilito
	delay_us(Tiempo_Dato);//10ms
	IO0CLR=MODO_SET;//Pongo en cero todos los bit
}

/*ENVIAR CADENA DE CARACTERES*/
void imprimir_lcd(unsigned char *Palabra, unsigned int linea)
{
if(linea==1)
{limpiar();
//Comienza el proceso de escribir los caracteres de la cadena
while(*Palabra!='\0')//Mientas no se haya llegado al fin de la cadena
	{IO0SET=(RS|*Palabra<<10);//Activo el pin de escribir y paso el caracter guardado en la primer posición
	 delay_us(10);//10us
 	 IO0SET=ENABLE;//Habilito
 	 delay_us(20);//20us
	 IO0CLR=ENABLE;//Desabilito
	 delay_us(50);//50us--> Hoja de dato aprox 40us
 	 IO0CLR=(RS|0x3FC00);//Pongo en bajo todos los pines
 	 Palabra++;}//Paso al siguiente caracter
}

else{
if(linea==2)
{limpiar();
 IO0SET=DIRECCION_LINEA_2;//Direccion de memoria a escribir y de allí cada caracter se desplaza +1
 delay_us(10);//10us
 IO0SET=ENABLE;//Habilito
 delay_us(30);//30us
 IO0CLR=ENABLE;//Desabilito
 delay_us(100);//100us
 IO0CLR=DIRECCION_LINEA_2;//Pongo en bajo el bit


//Comienza el proceso de escribir los caracteres de la cadena
while(*Palabra!='\0')//Mientas no se haya llegado al fin de la cadena
	{IO0SET=(RS|*Palabra<<10);//Activo el pin de escribir y paso el caracter guardado en la primer posición
	 delay_us(10);//10us
 	 IO0SET=ENABLE;//Habilito
 	 delay_us(20);//20us
	 IO0CLR=ENABLE;//Desabilito
	 delay_us(50);//50us--> Hoja de dato aprox 40us
 	 IO0CLR=(RS|0x3FC00);//Pongo en bajo todos los pines
 	 Palabra++;}//Paso al siguiente caracter
}}
}


void limpiar(void)
{IO0SET=LIMPIAR;//Limpio el lcd "00000001"
 delay_us(10);//10us
 IO0SET=ENABLE;//Habilito
 delay_us(30);//30us
 IO0CLR=ENABLE;//Desabilito
 delay_us(10);//10us
 IO0CLR=LIMPIAR;//Pongo en bajo el bit DB0
 delay_us(2000);//2ms--> Hoja de dato aprox 1,64ms
}

void refresh_lcd(unsigned char * primer_linea, unsigned char * segunda_linea)
{
    imprimir_lcd(primer_linea,1);
    imprimir_lcd(segunda_linea,2);


}


#ifndef SENSADO_H
#define SENSADO_H
#include "float_char.h"
#include "adc.h"
#include "delay.h"

#define MAX_TEMP 100 // Max temp == 3.3v

float *t_max, *t_min, *t_actual;
void sensado_start(void);
void sensado_init( float *, float *, float *);
#endif

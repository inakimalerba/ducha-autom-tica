#include "main.h"


int main()
{
    unsigned char init_msj[16]="Iniciando.......";
    unsigned char medidor_msj[16] = "10-----#------15";
    unsigned char detalles_msj[16] = "Set: ##  Act: ##";

    unsigned char enviar[5];

    unsigned char temp_max_c[5], temp_min_c[5], temp_actual_c[5];
    float temp_max = 0.0, temp_min = 0.0, temp_actual = 0.0;

    float porc;
    //inic_int();
    
    //Init salidas
    PINSEL0=Conf_Pines0;/*Pines LCD, LED, PWM, BOTONES, ADC*/
    IO0DIR=0x3FFF;
    //IO0DIR=0x1FFC00;

    inic_delay();
    //inic_lcd_8bit();
    pwm_init();

    //pwm_on(); //NO ESTA MAS 


    sensado_init(&temp_max, &temp_min, &temp_actual);
    sensado_start();
    
    //UART_Init();
    //PINSEL1=0x15400000;
    //UART_BaudRateConfig(UART_BAUD(9600));

    
    /*limpiar(); //limpio display
    imprimir_lcd(init_msj, 1);

    delay_ms(1000);
*/

	while(1)
	{

        sensado_start();
        //porc = (temp_max/3.3)*100;
        porc = temp_max;
        
        //float_char(temp_max/100, enviar,1);
	    //UART_StringSend(enviar);
        
        pwm_modul(1, porc); // RECORDAR QUE AHORA SON DOS PARAMETROS
        //TODO hacer dinamico
        //strncpy((unsigned char *)medidor_msj,"10-----#------57", sizeof(medidor_msj));

        //float_char(temp_max, temp_max_c);
        //float_char(temp_min, temp_min_c);
        //float_char(temp_actual, temp_actual_c);


        //strncpy((unsigned char *)detalles_msj, detalles_msj, sizeof(detalles_msj));

/*
        //refresh_lcd(medidor_msj,detalles_msj);
        limpiar();
//        imprimir_lcd(medidor_msj,1);
        imprimir_lcd(detalles_msj,2);

	//	IO0SET = 0xFFFFFFFF;
	//	delay_ms(1000);//ms
	//	IO0CLR = 0xFFFFFFFF;*/
		delay_ms(1000);
	}
	return 0;
}

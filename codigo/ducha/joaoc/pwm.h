#define PLOCK 0x00000400
#include "lpc2114.h"
#define PWMPRESCALE 60   //60 PCLK cycles to increment TC by 1 i.e 1 Micro-second 
#define PERIOD 20000 

void pwm_modul(int num, float porc);
void pwm_init(void);

#include"pwm.h"

void pwm_init(void)
{
    /*Assuming that PLL0 has been setup with CCLK = 60Mhz and PCLK also = 60Mhz.*/
    /*This is a per the Setup & Init Sequence given in the tutorial*/

    PINSEL0 |= (1<<1); // Select PWM1 output for Pin0.0
    PINSEL0 |= (1<<3); // Select PWM3 output for Pin0.1
    PWMPCR = 0x0; //Select Single Edge PWM - by default its single Edged so this line can be removed
    PWMPR = PWMPRESCALE-1; // 1 micro-second resolution
    PWMMR0 = 20000; // 20ms = 20k us - period duration
    PWMMR1 = 1000; // 1ms - pulse duration i.e width
    PWMMR3 = 1000; // 1ms - pulse duration i.e width
    PWMMCR = (1<<3) | (1<<1); // Reset PWMTC on PWMMR0 match
    PWMLER = (1<<3) | (1<<1) | (1<<0); // update MR0, MR1 and MR2
    PWMPCR = (1<<11) | (1<<9); // enable PWM output
    PWMTCR = (1<<1) ; //Reset PWM TC & PR

    //Now , the final moment - enable everything
    PWMTCR = (1<<0) | (1<<3); // enable counters and PWM Mode
}

void pwm_modul(int num, float porc1, float porc2)
{ 
            PWMMR1 = 1000+(porc1*5);
            PWMLER = (1<<1); 

            PWMMR3 = 1000+(porc2*5);
            PWMLER = (1<<3); 
}

#include "sensado.h"

void sensado_start(void){
    float valor, factor;
    unsigned char enviar[5];
    unsigned int adc;

    factor = MAX_TEMP / 3.3;

    adc = adc_salida(1);
	valor = (adc * 0.0033);
	*t_max = valor * factor;
    
    adc = adc_salida(2);
	valor = (adc * 0.0033);
	*t_min = valor * factor;
    
    adc = adc_salida(8);
	valor = (adc * 0.0033);
	*t_actual = valor * factor;

    //adc = adc_salida(16);
    //valor = (adc * 0.0033);
    //*t_actual = valor * factor;
    
//    float_char(valor, enviar);
//	UART_StringSend(enviar);

}

void sensado_init( float * max,  float * min,  float * actual){
    t_max = max;
    t_min = min;
    t_actual = actual;
}

#include "menulcd.h"
/*ARCHIVO LISTO*/

unsigned int menulcd(void)
{unsigned int tipo_de_soldadura, salir;

/*Comenzamos con los pasos del menu*/

do{
	tipo_de_soldadura=ventana1();//Devuelve si el usuario quiere estandar o personalizado

	if(tipo_de_soldadura!=0)
		{tipo_de_soldadura=ventana_psl();}//Se modifica los parametros

	salir=ventana_confirmar();//Si devuelve 1 sale y comienza la soldadura, sino de nuevo a la ventana 1
}while(salir==1);

return tipo_de_soldadura;
}


unsigned int ventana1(void)
{
unsigned int boton_select0, boton_OK0, entrada0;
/*boton_select0 ---> '0' boton izquierdo
               ---> '1' boton derecho
  boton_OK0---> '1' Apreto confirmacion
           ---> '0' Por defecto
  entrada0--->Guarda el estado de GPIO de las entradas pertinentes*/

unsigned char ventana_principal[]="Normal  Personal";//Pantalla de entrada
unsigned char simbolo1[]="  [^]          ";//Para la primer parte select Estandar
unsigned char simbolo2[]="          [^]  ";//Para la primer parte select Personalizado

/*Imprimo la primer ventana y la seleccion por defecto de estandar */

imprimir_lcd(ventana_principal, 1);//En la linea uno se imprime "Estandar Personalizado"
imprimir_lcd(simbolo1, 2);//En la linea dos el '^' va a estar en estandar por defecto
boton_select0=0;//Por defecto
boton_OK0=0;//Para reiniciar el estado

/*	_________________________________________
        |					|
	| Estandar           Personalizado	|
	|    ^                          	|  [BOTON 'OK']
	|_______________________________________|
	   [BOTON]                   [BOTON]
*/

/*Seleccion de que tipo de soldadura se va a realizar*/

	do{	entrada0=(IO0PIN&MASC_BOTONES);

		if((entrada0&(1<<Shift_IZQ))!=0)
			{imprimir_lcd(simbolo1, 2);
			 boton_select0=0;
			 while((IO0PIN&(1<<Shift_IZQ))!=0);}
		else{
		if((entrada0&(1<<Shift_DER))!=0)
			{imprimir_lcd(simbolo2, 2);
			 boton_select0=1;
			 while((IO0PIN&(1<<Shift_DER))!=0);}}

		if((entrada0&(1<<Shift_OK))!=0)
			{boton_OK0=1;
			 while((IO0PIN&(1<<Shift_OK))!=0);}
	}while(boton_OK0==0);

return boton_select0;}

unsigned int ventana_confirmar(void)
{unsigned int boton_select1, boton_OK1, entrada1;
/*boton_select1 ---> '0' boton izquierdo
               ---> '1' boton derecho
  boton_OK1---> '1' Apreto confirmacion
          ---> '0' Por defecto */
/*Imprimo la ventana de confirmar y la seleccion por defecto de si */
unsigned char confirmar[]="    Confirmar   ";//Para confirmar operacion de iniciar
unsigned char si_no[]=" <SI>      NO  ";//Indica que boton confirma y cual no
unsigned char no_si[]="  SI      <NO> ";//Indica que boton confirma y cual no
imprimir_lcd(confirmar, 1);
imprimir_lcd(si_no, 2);
boton_select1=0;//Por defecto
boton_OK1=0;//Para reiniciar el estado
/*	_________________________________________
        |					|
	|	      COMFIRMAR			|
	|    <SI>                       NO	|  [BOTON 'OK']
	|_______________________________________|
	   [BOTON]                   [BOTON]
*/
/*Seleccion de que tipo de soldadura se va a realizar*/
	do{	entrada1=(IO0PIN&MASC_BOTONES);

		if((entrada1&(1<<Shift_IZQ))!=0)
			{imprimir_lcd(si_no, 2);
			 boton_select1=0;
			 while((IO0PIN&(1<<Shift_IZQ))!=0);}
		else{
		if((entrada1&(1<<Shift_DER))!=0)
			{imprimir_lcd(no_si, 2);
			 boton_select1=1;
			 while((IO0PIN&(1<<Shift_DER))!=0);}}

		if((entrada1&(1<<Shift_OK))!=0)
			{boton_OK1=1;delay_ms(100);
			 while((IO0PIN&(1<<Shift_OK))!=0);}
	}while(boton_OK1==0);

return boton_select1;}

unsigned int ventana_psl(void)
{unsigned int boton_select, boton_OK, entrada, move, end;
/*boton_select ---> '0' boton izquierdo
               ---> '1' boton derecho
  boton_OK---> '1' Apreto confirmacion
          ---> '0' Por defecto
  entrada--> Tiene los estados de la GPIO de las entradas que me interesan*/
unsigned char ventana_etapa_A[]="     ETAPA A    ";//Indica que etapa se va a modificar
unsigned char ventana_etapa_B[]="     ETAPA B    ";//Indica que etapa se va a modificar
unsigned char ventana_etapa_C[]="     ETAPA C    ";//Indica que etapa se va a modificar
unsigned char ventana_etapa_D[]="     ETAPA D    ";//Indica que etapa se va a modificar
unsigned char modificar_siguiente[]=" <Modif>   Sig  ";//Para indicar boton modificar o pasar a siguiente
unsigned char siguiente_modificar[]="  Modif   <Sig> ";
/*	_________________________________________
        |					|
	|	        ETAPA 'N'		|
	|   <Modificar>             Siguiente	|  [BOTON 'OK']
	|_______________________________________|
	   [BOTON]                   [BOTON]
*/
move=0;
end=0;
do{	boton_select=0;
	boton_OK=0;

	switch(move)
		{case 0:{/*ETAPA 'A'*/
			 imprimir_lcd(ventana_etapa_A, 1);
			 imprimir_lcd(modificar_siguiente, 2);
			 break;}
		 case 1:{/*ETAPA B*/
		         imprimir_lcd(ventana_etapa_B, 1);
			 imprimir_lcd(modificar_siguiente, 2);
			 break;}
		 case 2:{/*ETAPA C*/
			 imprimir_lcd(ventana_etapa_C, 1);
			 imprimir_lcd(modificar_siguiente, 2);
			 break;}
		 case 3:{/*ETAPA D*/
			 imprimir_lcd(ventana_etapa_D, 1);
			 imprimir_lcd(modificar_siguiente, 2);
			 break;}
		 default: {end=1;boton_OK=1; boton_select=1;}
		}

/*Define que boton se selecciona y luego espera la confirmacion del boton 'OK'*/
	while(boton_OK==0)
		{entrada=(IO0PIN&MASC_BOTONES);

		if((entrada&(1<<Shift_IZQ))!=0)
			{imprimir_lcd(modificar_siguiente, 2);
			 boton_select=0;delay_ms(5);
			 while((IO0PIN&(1<<Shift_IZQ))!=0);}
		else{
		if((entrada&(1<<Shift_DER))!=0)
			{imprimir_lcd(siguiente_modificar, 2);
			 boton_select=1;delay_ms(5);
			 while((IO0PIN&(1<<Shift_DER))!=0);}}

		if((entrada&(1<<Shift_OK))!=0)
			{boton_OK=1;
			 delay_ms(100);
			 while((IO0PIN&(1<<Shift_OK))!=0);}
		}

/*Si se eligio modificar va la funcion que corrige los parametros*/
		if(boton_select==0)
			{modificar_param(move);}

move++;//Pasamos a la siguiente ventana
}while(end==0);

return 1;}

void modificar_param(unsigned int etapa)
{unsigned int boton_OK2, entrada2, aux, aux1, aux2;
 unsigned char temperatura[]=" TEMPERATURA[C] ";//Temperatura máxima de etapa
 unsigned char tiempo[]="   TIEMPO[s]    ";//Tiempo de etapa
 unsigned char signo[]=" [+]        [-] ";//Para indicar que boton, aumenta, cual disminuye
 unsigned char valor[2], help[2];
/*	_________________________________________
        |					|
	|	      Temperatura[C]		|
	|    +            150           -	|  [BOTON 'OK']
	|_______________________________________|
	   [BOTON]                   [BOTON]*/
/*	_________________________________________
        |					|
	|	       Tiempo[s]		|
	|    +          75              -	|  [BOTON 'OK']
	|_______________________________________|
	   [BOTON]                   [BOTON]*/


/*Selección de la temperatura*/
 	switch(etapa)
	{case 0: {aux=Temperatura_A_std;
	          help[0]='0';
	          help[1]='0';
	          break;}//Modificacion de temp de etapa A, defino como predeterminada la estandar
	 case 1: {aux=Temperatura_B_std;
		  aux2=param_psl[3];
	          aux1=aux2/100;
	          aux2=aux2/10;
	          help[0]=48+aux1;
	          help[1]=48+(aux2-(aux1*10));
		  break;}//Mantiene la temperatura anterior
	 case 2: {aux=Temperatura_C_std;
		  aux2=param_psl[6];
	          aux1=aux2/100;
	          aux2=aux2/10;
	          help[0]=48+aux1;
	          help[1]=48+(aux2-(aux1*10));
                  break;}//Modificacion de temp de etapa C, defino como predeterminada la estandar
	 case 3: {aux=Temperatura_Seguridad_std;
		  help[0]='0';
		  help[1]='0';
		  break;}//Modificacion de temp de seguridad de la etapa D
	 default: aux=0;}

if(aux!=0)//Verifica si la etapa tiene permiso de cambiar temperatura
{
/*Se encargar de pasar la informacion a unsigned char para LCD*/
	aux1=aux/100;
	aux2=aux/10;
	signo[base]=48+aux1;
	signo[base+1]=48+(aux2-(aux1*10));
        signo[base+2]=48+(aux-(aux2*10));

/*Imprime la informacion en la pantalla TEMPERATURA*/
	imprimir_lcd(temperatura, 1);
	imprimir_lcd(signo, 2);
	boton_OK2=0;


while(boton_OK2==0)
		{entrada2=(IO0PIN&MASC_BOTONES);

		if((entrada2&(1<<Shift_IZQ))!=0)
			{
		/*Sube el valor de la temperatura*/
		        signo[base+1]++;
			if((signo[base+1]>'5')&&(signo[base]>='2'))
				{signo[base+1]='0'; signo[base]='0'; signo[base+2]='0';}
			else{if((signo[base+1]>'9')&&(signo[base]<'2'))
				{signo[base+1]='0';signo[base]++;signo[base+2]='0';}}
			imprimir_lcd(signo, 2);delay_ms(5);
			while((IO0PIN&(1<<Shift_IZQ))!=0);}//Imprime [+]    TEMP    [-]

		if((entrada2&(1<<Shift_DER))!=0)
		{
		/*Baja el valor del temperatura*/
			signo[base+1]--;
			if(signo[base+1]<'0' && signo[base]>help[0])
				{signo[base]--; signo[base+1]='9'; signo[base+2]='0';}
			else{
			if(signo[base+1]<help[1] && signo[base]<=help[0])
				{signo[base+2]='0'; signo[base+1]=help[1];signo[base]=help[0];}}


			 imprimir_lcd(signo, 2);delay_ms(5);
		         while((IO0PIN&(1<<Shift_DER))!=0);}//Imprime [+]    TEMP    [-]



		/*Baja el valor de la temperatura*/
			/*signo[base+1]--;
			if(signo[base+1]<'0')
				{if(signo[base]<='0')
					{signo[base+2]='0'; signo[base+1]='0';signo[base]='0';}
				  else{signo[base]--; signo[base+1]='9'; signo[base+2]='0';}}

			imprimir_lcd(signo, 2);delay_ms(3);
		         while((IO0PIN&(1<<Shift_DER))!=0);}//Imprime [+]    TEMP    [-]
		*/
		 if((entrada2&(1<<Shift_OK))!=0)
			{boton_OK2=1;delay_ms(100);while((IO0PIN&(1<<Shift_OK))!=0);}//Confirmacion
		 }//Fin while

/*Para limitar el tiempo*/
help[0]=signo[base];
help[1]=signo[base+1];
/*Guardo la informacion de temperatura*/
aux=(int)signo[base+2];//Unidad
aux1=(int)signo[base+1];//Decena
aux2=(int)signo[base];//Centena
aux=aux-48;
aux1=(aux1-48)*10;
aux2=(aux2-48)*100;
}

/*Levanto la informacion final y las paso a los datos de main*/
switch(etapa)
{ case 0: param_psl[3]=aux+aux1+aux2;
	  valor[0]='0';
          valor[1]='0';
          aux=1;
	  break;

  case 1: param_psl[6]=aux+aux1+aux2;
	  aux=param_psl[3];
	  aux1=aux/100;
	  aux2=aux/10;
	  valor[0]=48+aux1;
	  valor[1]=48+(aux2-(aux1*10));
          aux=1;
	  break;

   case 2: param_psl[4]=aux+aux1+aux2;
          aux=param_psl[6];
	  aux1=aux/100;
	  aux2=aux/10;
	  valor[0]=48+aux1;
	  valor[1]=48+(aux2-(aux1*10));
          aux=1;
	  break;
  case 3: param_psl[5]=aux+aux1+aux2;
	  aux=0;
          break;
  default: aux=0;}

	if(aux!=0)
		{
         	help[0]=(help[0]-valor[0])+48;
	        help[1]=(help[1]-valor[1])+48;
		if(help[1]<'0')
			{help[1]='0';}
		signo[base]=help[0];
     		signo[base+1]=help[1];
     		signo[base+2]='0';

		imprimir_lcd(tiempo, 1);//Imprime la linea una con la palabra TEMPERATURA

		imprimir_lcd(signo, 2);

		boton_OK2=0;

		while(boton_OK2==0)
		{entrada2=(IO0PIN&MASC_BOTONES);
		 if((entrada2&(1<<Shift_IZQ))!=0)
			{
		/*Sube el valor de la tiempo*/
		        signo[base+1]++;
			if(signo[base+1]>'9')
				{signo[base+1]='0';signo[base]++; signo[base+2]='0';}
			if(signo[base]>'9')
				{signo[base+1]=help[1]; signo[base]=help[0]; signo[base+2]='0';}

			imprimir_lcd(signo, 2);delay_ms(5);
			while((IO0PIN&(1<<Shift_IZQ))!=0);}//Imprime [+]    TEMP    [-]

		 if((entrada2&(1<<Shift_DER))!=0)
			{
		/*Baja el valor del tiempo*/
			signo[base+1]--;
			if(signo[base+1]<'0' && signo[base]>help[0])
				{signo[base]--; signo[base+1]='9'; signo[base+2]='0';}
			else{
			if(signo[base+1]<help[1] && signo[base]<=help[0])
				{signo[base+2]='0'; signo[base+1]=help[1];signo[base]=help[0];}}


			 imprimir_lcd(signo, 2);delay_ms(5);
		         while((IO0PIN&(1<<Shift_DER))!=0);}//Imprime [+]    TEMP    [-]

		  if((entrada2&(1<<Shift_OK))!=0)
			{boton_OK2=1;delay_ms(100);while((IO0PIN&(1<<Shift_OK))!=0);}//Confirmacion
		 }//Fin while
		}

/*Levanto la informacion final y las paso a los datos de main*/
if(aux!=0)
{aux=(int)signo[base+2];//Unidad
aux1=(int)signo[base+1];//Decena
aux2=(int)signo[base];//Centena
aux=aux-48;
aux1=(aux1-48)*10;
aux2=(aux2-48)*100;
param_psl[etapa]=aux+aux1+aux2;}
}
